(function (global, factory) {	// here the function is defined...
    if (typeof define === "function" && define.amd) {
        define(['module'], factory);
    } else if (typeof exports !== "undefined") {
        factory(module);
    } else {
        var mod = {
            exports: {}
        };
        factory(mod); // The factory creates the object
        global.TimelineChart = mod.exports;
    }
})(this, function (module) {	// ...and here that function is called, ...
								// The second argument is the factory function
    'use strict';

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();
    
	var updateTimeline;

	var TimelineChart = function () {

        function TimelineChart(element, data, opts) {
			// element is the document element where the timeline chart is to be drawn in
			// data is the timeline chart data
			// opts are options
            _classCallCheck(this, TimelineChart);

            var self = this;

            // add the 'timeline-chart' class to the element, so it becomes sensitive to timeline-chart styles
            element.classList.add('timeline-chart');

            var options = this.extendOptions(opts);

            // Flatten the array of timelines into an array of times only: compute the union of all timelines
            var allElements = data.reduce(function (agg, e) {
                return agg.concat(e.data);
            }, []);

            // Find the time span of the union of all timelines
            var minDt = d3.min(allElements, this.getPointMinDt);
                minDt = new Date(minDt.getTime() - minDt.getTime() % (24*3600000));
            //var maxDt = d3.max(allElements, this.getPointMaxDt);
            var maxDt = new Date(minDt.getTime()+24*3600000);

            // Set the width and height of the of the window (element)
            var elementWidth = options.width || element.clientWidth;
            var elementHeight = options.height || element.clientHeight;

            var margin = {
                top: 0,
                right: 0,
                bottom: 20,
                left: 0
            };

            var width = elementWidth - margin.left - margin.right;
            var height = elementHeight - margin.top - margin.bottom;

            // Set the width of the columns
            var groupWidth = 200;

            var x = d3.time.scale.utc().domain([minDt, maxDt]).range([groupWidth, width]);
//            var x = d3.time.scale().domain([minDt, maxDt]).range([groupWidth, width]); // if you want to use local time

            var xAxis = d3.svg.axis().scale(x).orient('bottom').tickSize(-height);

            var zoom = d3.behavior.zoom().x(x).on('zoom', zoomed).scaleExtent([1 / 2048, 1024]);

            var svg = d3.select(element)
                      .append('svg').attr('width', width + margin.left + margin.right)
                                    .attr('height', height + margin.top + margin.bottom)
                      .append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
                      .call(zoom);

            svg.append('defs')
               .append('clipPath').attr('id', 'chart-content')
               .append('rect').attr('x', groupWidth).attr('y', 0).attr('height', height).attr('width', width - groupWidth);

            svg.append('rect')
               .attr('class', 'chart-bounds').attr('x', groupWidth).attr('y', 0).attr('height', height)
                                             .attr('width', width - groupWidth);

            svg.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + height + ')').call(xAxis);

            var groupHeight = height / data.length;
            var groupSection = svg.selectAll('.group-section')
                                  .data(data).enter()
                                  .append('line').attr('class', 'group-section')
                                                 .attr('x1', 0).attr('x2', width)
                                                 .attr('y1', function (d, i) {return groupHeight * (i + 1);})
                                                 .attr('y2', function (d, i) {return groupHeight * (i + 1);});

            var groupLabels = svg.selectAll('.group-label')
                                 .data(data).enter()
                                 .append('text').attr('class', 'group-label')
                                                .attr('x', 0)
                                                .attr('y', function (d, i) {return groupHeight * i + groupHeight / 2 + 5.5;})
                                                .attr('dx', '0.5em').text(function (d) {return d.label;});

            var lineSection = svg.append('line').attr('x1', groupWidth)
                                                .attr('x2', groupWidth)
                                                .attr('y1', 0)
                                                .attr('y2', height)
                                                .attr('stroke', 'black');

            var intervalBarHeight = 0.8 * groupHeight;
            var intervalBarMargin = (groupHeight - intervalBarHeight) / 2;

            var groupIntervalItems = svg.selectAll('.group-interval-item')
                                        .data(data).enter()
                                        .append('g').attr('clip-path', 'url(#chart-content)')
                                                    .attr('class', 'item')
                                                    .attr('transform', function (d, i) {return 'translate(0, ' + groupHeight * i + ')';})
                                        .selectAll('.dot').data(function (d)
                                                               {return d.data.filter(function (_)
																                     {return _.type === TimelineChart.TYPE.INTERVAL;
																				     });
                                                               })
                                        .enter();
            
            var intervals = groupIntervalItems.append('rect').attr('class', withCustom('interval'))
                                                             .attr('width', function (d)
                                                                            {return Math.max(options.intervalMinWidth, x(d.to) - x(d.from));
                                                                            })
                                                             .attr('height', intervalBarHeight)
                                                             .attr('y', intervalBarMargin)
                                                             .attr('x', function (d) {return x(d.from);});
            

            var intervalTexts = groupIntervalItems.append('text').text(function (d) {return d.label;})
                                                                 .attr('fill', 'white')
                                                                 .attr('class', withCustom('interval-text'))
                                                                 .attr('y', groupHeight / 2 + 5).attr('x', function (d) {return x(d.from);});

            var groupDotItems = svg.selectAll('.group-dot-item').data(data).enter()
                                   .append('g').attr('clip-path', 'url(#chart-content)')
                                               .attr('class', 'item')
                                               .attr('transform', function (d, i) {return 'translate(0, ' + groupHeight * i + ')';})
                                   .selectAll('.dot').data(function (d)
                                                           {return d.data.filter(function (_)
															                     {return _.type === TimelineChart.TYPE.POINT;
																			     });
                                                           })
                                    .enter();

            var dots = groupDotItems.append('circle')
                                    .attr('class', withCustom('dot'))
                                    .attr('cx', function (d) {return x(d.at);})
                                    .attr('cy', groupHeight / 2)
                                    .attr('r', 5);
                                    
            setTimeBar(new Date()); // This should defer setting the bar until at the whole second

            if (options.tip) {
                if (d3.tip) {
                    var tip = d3.tip().attr('class', 'd3-tip').html(options.tip);
                    svg.call(tip);
                    dots.on('mouseover', tip.show).on('mouseout', tip.hide);
                } else {
                    console.error('Please make sure you have d3.tip included as dependency (https://github.com/Caged/d3-tip)');
                }
            }
            zoomed();

            function zoomed() {
                if (self.onVizChangeFn && d3.event) {
                    self.onVizChangeFn.call(self, {
                        scale: d3.event.scale,
                        translate: d3.event.translate,
                        domain: x.domain()
                    });
                }

                svg.select('.x.axis').call(xAxis);

                svg.selectAll('circle.dot').attr('cx', function (d) {
                    return x(d.at);
                });
                svg.selectAll('rect.interval').attr('x', function (d) {
                    return x(d.from);
                }).attr('width', function (d) {
                    return Math.max(options.intervalMinWidth, x(d.to) - x(d.from));
                });
                svg.selectAll("line.now").attr('x1', function (d) { return x(new Date(Date.now())) })
                                         .attr('x2', function (d) { return x(new Date(Date.now())) });
                                         
                svg.selectAll('.interval-text').attr('x', function (d) {
                    var positionData = getTextPositionData.call(this, d);
                    if (positionData.upToPosition - groupWidth - 10 < positionData.textWidth) {
                        return positionData.upToPosition;
                    } else if (positionData.xPosition < groupWidth && positionData.upToPosition > groupWidth) {
                        return groupWidth;
                    }
                    return positionData.xPosition;
                }).attr('text-anchor', function (d) {
                    var positionData = getTextPositionData.call(this, d);
                    if (positionData.upToPosition - groupWidth - 10 < positionData.textWidth) {
                        return 'end';
                    }
                    return 'start';
                }).attr('dx', function (d) {
                    var positionData = getTextPositionData.call(this, d);
                    if (positionData.upToPosition - groupWidth - 10 < positionData.textWidth) {
                        return '-0.5em';
                    }
                    return '0.5em';
                }).text(function (d) {
                    var positionData = getTextPositionData.call(this, d);
                    var percent = (positionData.width - options.textTruncateThreshold) / positionData.textWidth;
                    if (percent < 1) {
                        if (positionData.width > options.textTruncateThreshold) {
                            return d.label.substr(0, Math.floor(d.label.length * percent)) + '...';
                        } else {
                            return '';
                        }
                    }

                    return d.label;
                });

                function getTextPositionData(d) {
                    this.textSizeInPx = this.textSizeInPx || this.getComputedTextLength();
                    var from = x(d.from);
                    var to = x(d.to);
                    return {
                        xPosition: from,
                        upToPosition: to,
                        width: to - from,
                        textWidth: this.textSizeInPx
                    };
                }
            }
            
			var doTrackTime = false;
			
			function trackTime() {
				doTrackTime = ! doTrackTime;
			}
			
			function setTimeBar(t) {
		
				svg.selectAll('.nowItem').remove();
				
				var tt = t.getTime();
				tt = tt - tt % 1000;
				var nowItem = svg.selectAll('.group-now-item')
								 .data([tt]).enter()
								 .append('g').attr('clip-path', 'url(#chart-content)')
								 .attr('class', 'nowItem');
		
				var now = nowItem.append('line').attr('class', withCustom('now'))
								 .attr('x1', x(tt))
								 .attr('x2', x(tt))
								 .attr('y1', 0).attr('y2', height)
								 .attr('stroke-width', 2)
								 .attr('style', 'stroke: red');
				if (doTrackTime) {
					//console.log("zoom: translate:", zoom.translate());
					//console.log("zoom: scale:", zoom.scale(), 1 / zoom.scale());
					//console.log("zoom: scaleExtent:", zoom.scaleExtent());
					//console.log("zoom: size:", zoom.size());
					//console.log("zoom: x:", zoom.x());
					//console.log("zoom: y:", zoom.y());
					var deltaT = x.domain()[1]-x.domain()[0];
					var currentMinDt = tt - 0.15 * deltaT;
					var scaleFactor = (maxDt - minDt) / (width - groupWidth);
					var currentScaleFactor = deltaT / (x.range()[1]-x.range()[0]);
					//console.log("delta-T:", deltaT);
					//console.log("original deltaT:", maxDt - minDt, "original delta-T / zoom.scale():", (maxDt - minDt) / zoom.scale())
					//console.log("T-begin:", new Date(currentMinDt));
					//console.log("Original scale factor:", scaleFactor);
					//console.log("Scale factor:", currentScaleFactor);
					//console.log("Original scalefactor / zoom.scale:", scaleFactor / zoom.scale());
					//console.log("Tranlate:", (minDt - currentMinDt) / currentScaleFactor - (scaleFactor / currentScaleFactor - 1) * groupWidth);
					//console.log("scaled time:", x(tt));
					//console.log("======================");
					//clearInterval(myVar);
					var scale = (maxDt.getTime() - minDt.getTime()) / width;
					zoom.translate([(minDt - currentMinDt) / currentScaleFactor - (scaleFactor -  currentScaleFactor) / currentScaleFactor * groupWidth, 0]);
					zoomed();
				}
			};
	
			
			function updateTimeBar()
			{
				setTimeBar(new Date(Date.now()));
			}
			
			var myVar = setInterval(updateTimeBar, 1000);
			self.updateTimeline = trackTime;

        }

		function withCustom(defaultClass) {
				return function (d) {
							return d.customClass ? [d.customClass, defaultClass].join(' ') : defaultClass;
						};
			}
		
        _createClass(TimelineChart, [{
            key: 'extendOptions',
            value: function extendOptions() {
                var ext = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

                var defaultOptions = {
                    intervalMinWidth: 8, // px
                    tip: undefined,
                    textTruncateThreshold: 30
                };
                Object.keys(ext).map(function (k) {
                    return defaultOptions[k] = ext[k];
                });
                return defaultOptions;
            }
        }, {
            key: 'getPointMinDt',
            value: function getPointMinDt(p) {
                return p.type === TimelineChart.TYPE.POINT ? p.at : p.from;
            }
        }, {
            key: 'getPointMaxDt',
            value: function getPointMaxDt(p) {
                return p.type === TimelineChart.TYPE.POINT ? p.at : p.to;
            }
        }, {
            key: 'onVizChange',
            value: function onVizChange(fn) {
                this.onVizChangeFn = fn;
                return this;
            }
        //}, {
            //key: 'updateTimeline',
            //value: self.updateTimeline
        }]);

        return TimelineChart;
    }();

    TimelineChart.TYPE = {
        POINT: Symbol(),	// This creates two primitive symbols, TimelineChart.TYPE.POINT and ...INTERVAL
        INTERVAL: Symbol()	// It is the call to Symbol() which creates these two primitive symbols
    };

    module.exports = TimelineChart;

}); // ...that function invocation runs up to here.

//# sourceMappingURL=timeline-chart.js.map
