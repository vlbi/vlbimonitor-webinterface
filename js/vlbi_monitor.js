"use strict";

function renderUserId(outputid) {
	var username = window.localStorage.getItem('username');

	var html = "";
	html += username + "<br>";
	html += "<a href='/login.html' id='logout'>logout</a>";

	$(outputid).html(html);

	$('#logout').click(function () {
		Cookies.remove('sessionid');
	});
}


function renderMenu() {
	var menuitems = [
		{text:"Overview", 		link:"/overview.html"},
		{text:"Details", 		link:"/matrix.html"},
		{text:"Weather", 		link:"/weather.html"},
		{text:"Schedule", 		link:"/schedule.html"},
		{text:"World map", 		link:"/map.html"},
		{text:"Celestial map", 	link:"/celestial.html"},
		{text:"Messages", 		link:"/messages.html"},
		{text:"Issues", 		link:"https://bitbucket.org/vlbi/vlbimonitor-webinterface/issues?status=new&status=open"},
		{text:"Settings", 		link:"/settings.html"}
		//{text:"Logs", link:"/logs.html"}
	];

	$.each(menuitems.reverse(),function(i,menuitem) {
		var newitem = $("<li>");
		var link = $("<a>").attr("href",menuitem.link).html(menuitem.text);
		newitem.append(link);
		newitem.insertAfter("li#logo");
	});
}

// passing a (mutable) array in to get both the value and possible warnings out is a slightly awkward workaround for not having tuple return capabilities in js
function getCurrentExperiment() {
	if (datastore.schedule.$GLOBAL == undefined) return undefined;
	return {
		StartTime: datastore.schedule.$GLOBAL['exper_nominal_start'],
		StopTime: datastore.schedule.$GLOBAL['exper_nominal_stop'],
		Name: datastore.schedule.$GLOBAL['exper_name']
	};
}

function getCurrentScheduleItem() {
	if (datastore.schedule.$GLOBAL == undefined) return undefined;

  let now = new Date().getTime();
	for (let scanname in datastore.schedule.$SCHED) {
		let scandata = datastore.schedule.$SCHED[scanname];
		if (scandata.start < now && scandata.stop > now) {
			return {
				ScanId: scanname,
				Source: scandata.source,
				StartTime: scandata.start,
				StopTime: scandata.stop,
			};
		}
	}
	return undefined;
}


// extract next (the scan with the lowest start time that is still in the future)
function getNextScheduleItem() {
	let vex = vexstore.cur
	if (vex.$SCHED == undefined) vex = vexstore.next
	if (vex.$SCHED == undefined) return {undefined, undefined};

	let next = undefined;
	const now = new Date().getTime();
	let nextstart = new Date("3000-01-01T00:00:00Z").getTime()
	for (let scanid in vex.$SCHED) {
		let scandata = vex.$SCHED[scanid];
		// convert strings to js Date objects
		let start = scandata.start;
		if(start > now && start < nextstart) {
			next = {
				ScanId: scanid,
				Source: scandata.source,
				StartTime: scandata.start,
				StopTime: scandata.stop,
			};
			nextstart = next.StartTime;
		}
	}
	return {next, vex};
}


function radecdifference(to, from) {
	// console.log("comparing "+from+" to "+to);
	if(from == undefined) return "---";
	if(to == undefined) return "---";
	var isplit = from.search(/[^^][+-]/) + 1;
	var fromra = from.slice(0, isplit);
	var fromdec = from.slice(isplit);
	var isplit = to.search(/[^^][+-]/) + 1;
	var tora = to.slice(0, isplit);
	var todec = to.slice(isplit);
	return (tora-fromra).toString()+(todec-fromdec).toString();
}


// helper function to parse the url and get a parameter
function getURLParameter(name) {
	var url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function getCategory(variablename) {
	// console.log("attempting to split ");
	// console.log(variablename);
	return variablename.split(FieldSeparator,1)[0];
}

/** This expects keyvalue pairs in which each key contains at least one ':'.
    The part before the : is split of and all rows are grouped by the category name. */
function groupByCategory(data) {
	var groups = {};
	$.each(data, function (varname, subdata) {
		group = getCategory(varname);	 			// get the group of one this row
		if(!(group in groups)) groups[group] = {};	// create an empty subset for this group
		groups[group][varname] = subdata;			// insert the k,f tuple in the new subgroup
	});

	return groups;
}


/* slightly stupid solution to make the ui more responsive. 
   Long for loops may freeze the ui on some browsers. By sleeping for a 
   few ms this can be prevented.
 */
function each_interleaved(ary,timeout, callback, done) {
	var keys = Object.getOwnPropertyNames(ary);
	var i = 0;
	function next() {
		if(i < keys.length) {
			var res = callback(keys[i],ary[keys[i]]);
			if(res===false) return; // early stop by not scheduling next
			i++;
			setTimeout(next,timeout);
		} else {
			if(typeof(done)=="function"){
				done();	
			}
		}
	}

	next();
}


function showMessage(msgtext,duration,buttontext,callback) {
	// create new dom 
	 var snackbardiv = $("<div>").attr("class","mdl-js-snackbar mdl-snackbar")
		.append($("<div>").attr("class","mdl-snackbar__text"))
		.append($("<button>").attr("class","mdl-snackbar__action").attr("type","button"))
		.appendTo("body");
	// strip the jquery
	snackbardiv = snackbardiv[0];
	
    var snackbar = new MaterialSnackbar(snackbardiv);
    // console.log(snackbar)
    snackbar.showSnackbar({
      	message: msgtext,
      	timeout: duration||5000,
      	actionHandler: callback,
      	actionText: buttontext
    });
}


function sprintTime(seconds, longform) {
	var t = Math.round(seconds / 31536000);
	if (Math.abs(t) > 1) { return t + (longform?" year(s)":"y"); }
	t = Math.round(seconds / 2592000);
	if (Math.abs(t) > 1) { return t + (longform?" month(s)":"M"); }
	t = Math.round(seconds / 86400);
	if (Math.abs(t) > 1) { return t + (longform?" day(s)":"d"); }
	t = Math.round(seconds / 3600);
	if (Math.abs(t) > 1) { return t + (longform?" hour(s)":"h"); }
	t = Math.round(seconds / 60);
	if (Math.abs(t) > 1) { return t + (longform?" minute(s)":"m"); }
	return Math.round(seconds) + (longform?" second(s)":"s");
}

function uniqueid()
{
	return Math.random().toString(36).substr(2, 9);
}


/* Google Analytics tracking */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');


if(window.location.hostname=="vlbimon1.science.ru.nl") {
	ga('create', 'UA-97032357-1', 'auto');	
	ga('send', 'pageview');
}
if(window.location.hostname=="vlbimon2.science.ru.nl") {
	ga('create', 'UA-97032357-2', 'auto');	
	ga('send', 'pageview');
}
if(window.location.hostname=="casdev.science.ru.nl") {
	ga('create', 'UA-97032357-3', 'auto');	
	ga('send', 'pageview');
}


