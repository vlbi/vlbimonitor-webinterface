/**
 * Astronomical coordinate conversion routines
 **/

var radians = Math.PI / 180;
var π = Math.PI;
var degrees = 180 / π;

/* splitRaDec takes a string of the form "+52.5684+5.59453" in degress and converts it to a tuple of floats */
function splitRaDec(str) {
    var regex = /^([+-]?[0-9]+\.[0-9]+)([+-]?[0-9]+\.[0-9]+)$/;
    var match = regex.exec(str);
    
    if(!match) {
        console.warn("string is not a radec!", str);
        return [0,0];
    }

    return [Number(match[1]),Number(match[2])];
}


/* horizontal2equatorial converts azalt coordinates to radec coordinates. using a lat/lan and the current Date() */
horizontal2equatorial = function(az, alt, lat, lon) {
    var sinAz = Math.sin(az-Math.PI);
    var cosAz = Math.cos(az-Math.PI);
    var sinLat = Math.sin(lat);
    var cosLat = Math.cos(lat);
    var sinAlt = Math.sin(alt);
    var cosAlt = Math.cos(alt);
    var tanAlt = Math.tan(alt);

    var h = Math.atan2(sinAz, (cosAz * sinLat + tanAlt * cosLat));
    var dec = Math.asin(sinLat * sinAlt - cosLat * cosAlt * cosAz);

    var last = (gast(new Date())*radians + lon);
    var ra  = last-h; 

    return [ra,dec];
}


function antipode(position) {
	return [position[0] + 180, -position[1]];
}


function solarRaDecPosition(time) {
	var centuries = (time - Date.UTC(2000, 0, 1, 12)) / 864e5 / 36525; // since J2000
	return [ solarRightAscension(centuries) * degrees ,
			 solarDeclination(centuries) * degrees           ];
}

function solarPosition(time) {
	var centuries = (time - Date.UTC(2000, 0, 1, 12)) / 864e5 / 36525; // since J2000
	// console.log("time: "+time)
	// console.log("d3.time.day.utc.floor(time): "+d3.time.day.utc.floor(time))
    var longitude = (d3.time.day.utc.floor(time) - time) / 864e5 * 360 - 180;
    // console.log("longitude: "+longitude)
	return [
		longitude - equationOfTime(centuries) * degrees,
			  solarDeclination(centuries) * degrees
				  ];
}

// Equations based on NOAA’s Solar Calculator; all angles in radians.
// http://www.esrl.noaa.gov/gmd/grad/solcalc/

function equationOfTime(centuries) {
	var e = eccentricityEarthOrbit(centuries),
	    m = solarGeometricMeanAnomaly(centuries),
	    l = solarGeometricMeanLongitude(centuries),
	    y = Math.tan(obliquityCorrection(centuries) / 2);
	y *= y;
	return y * Math.sin(2 * l)
		- 2 * e * Math.sin(m)
		+ 4 * e * y * Math.sin(m) * Math.cos(2 * l)
		- 0.5 * y * y * Math.sin(4 * l)
		- 1.25 * e * e * Math.sin(2 * m);
}

function solarDeclination(centuries) {
	return Math.asin(Math.sin(obliquityCorrection(centuries)) * Math.sin(solarApparentLongitude(centuries)));
}
function solarRightAscension(centuries) {
	return Math.atan2(Math.cos(obliquityCorrection(centuries))*Math.sin(solarApparentLongitude(centuries)),Math.cos(solarApparentLongitude(centuries)));
}

function solarApparentLongitude(centuries) {
	return solarTrueLongitude(centuries) - (0.00569 + 0.00478 * Math.sin((125.04 - 1934.136 * centuries) * radians)) * radians;
}

function solarTrueLongitude(centuries) {
	return solarGeometricMeanLongitude(centuries) + solarEquationOfCenter(centuries);
}

function solarGeometricMeanAnomaly(centuries) {
	return (357.52911 + centuries * (35999.05029 - 0.0001537 * centuries)) * radians;
}

function solarGeometricMeanLongitude(centuries) {
	var l = (280.46646 + centuries * (36000.76983 + centuries * 0.0003032)) % 360;
	return (l < 0 ? l + 360 : l) / 180 * π;
}

function solarEquationOfCenter(centuries) {
	var m = solarGeometricMeanAnomaly(centuries);
	return (Math.sin(m) * (1.914602 - centuries * (0.004817 + 0.000014 * centuries))
			+ Math.sin(m + m) * (0.019993 - 0.000101 * centuries)
			+ Math.sin(m + m + m) * 0.000289) * radians;
}

function obliquityCorrection(centuries) {
	return meanObliquityOfEcliptic(centuries) + 0.00256 * Math.cos((125.04 - 1934.136 * centuries) * radians) * radians;
}

function meanObliquityOfEcliptic(centuries) {
	return (23 + (26 + (21.448 - centuries * (46.8150 + centuries * (0.00059 - centuries * 0.001813))) / 60) / 60) * radians;
}

function eccentricityEarthOrbit(centuries) {
	return 0.016708634 - centuries * (0.000042037 + 0.0000001267 * centuries);
}

// Greenwich apparent sidereal time
// Equations bosed on http://aa.usno.navy.mil/faq/docs/GAST.php

function gast(now)
{
	var year   = now.getUTCFullYear();
	var month  = now.getUTCMonth() + 1;
	var day    = now.getUTCDate();
	var hour   = now.getUTCHours();
	var minute = now.getUTCMinutes();
	var second = now.getUTCSeconds();

	var JD = gregorianToJulianDay(year, month, day, hour, minute, second);
	var JD0 = gregorianToJulianDay(year, month, day, 0, 0, 0);

	var D = JD - 2451545.0;
	var D0 = JD0 - 2451545.0;
	var T = D / 36525;
	var H = 24. * (JD - JD0);

    // Greenwich mean sidereal time
	var GMST = 6.697374558 + 0.06570982441908 * D0 + 1.00273790935 * H + 0.000026 * T * T;

	// The following are all expressed in degrees
	var Omega = 125.04 - 0.052954 * D; // Longitude of the ascending node of the Moon
	var L = 280.47 + 0.98565 * D; // Mean Longitude of the Sun
	var epsilon = 23.4393 - 0.0000004 * D; // Obliquity
	var dpsi = -0.000319 * Math.sin(Omega * radians) - 0.000024 * Math.sin(2 * L * radians);

	// Compute equation of the equinoxes
	var eqeq = dpsi * Math.cos(epsilon * radians);

	GAST = ((GMST + eqeq) * 360. / 24.) % 360.;

	return GAST;
}

function gregorianToJulianDay(year, month, day, hour, minute, second)
{
	var Y = 0;
	var M = 0;
	var d = day + ((hour * 3600.) + (minute * 60.) + second) / (3600 * 24);

	if (month==1 || month==2)
	{
		Y = year - 1;
		M = month + 12;
	}
	else
	{
		Y = year;
		M = month;
	}

	var a = Math.floor(Y / 100);
	var b = 2 - a + Math.floor(a / 4);

	return Math.floor(365.25 * (Y + 4716)) + Math.floor(30.6001 * (M + 1)) + d + b - 1524.5;
}
