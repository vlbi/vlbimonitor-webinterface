/*
A Vexfile consists of three types of declarations:
  1. named Sections
  2. named Paragraphs
  3. named Values

A Vexfile is line-based; a complete line can be a commented out by a leading '*' character.

One line can contain multiple declarations; each declaration ends with a ';' character.

Declarations can span multiple lines, i.e. a number of unterminated lines followed by a terminated line.

A Section declaration start with a '$' symbol.

A paragraph declaration start with identifier 'def' or 'scan' and ends with identifier 'enddef' or 'endscan', respectively.

Section and Paragraph names are unique, Value names may be repeated.

A Vexfile is loaded into nested objects in which Sections and Paragraphs are objects themselves.  Each nesting level in the object (Vexfile, Section, or Paragraph) can contain named Values (key-value pairs).  The value in a key-value pair is of type 'string' (in single-value cases) or of type 'array' (in multi-value cases).
*/

//import { Md5 } from 'md5'

// parse vexfile text into a js object
// filename is passed for info purposes only
const vexparser = function VexfileParser(text, filename) {
  // output
  let hash = ""
  const stack = []
  let md5sum = Md5.md5sum(text)
  let obj = { filename, md5sum }

  // input
  let lf = '\n'
  // msdos file format, sigh...
  if (text[text.indexOf('\n')-1] === '\r') lf = '\r\n'
  const lines = text.split(lf)

  let i
  let k, v, fullline, line, name, prev = '', pardef = ''
  let sublines
  const warn = err => console.log(`parse error: ${filename}(${i}): ${err}`)

  for (i = 0; i < lines.length; i++) {
    if (lines[i].length == 0) continue // empty
    if (lines[i][0] == '*') continue // comment
    // line continuation
    fullline = prev + lines[i].trim()
    prev = ''

    sublines = fullline.split(';')
    while ( (line = sublines.shift()) != undefined ) {
      line = line.trim()
      if (line.length == 0) continue // empty
      if (line[0] == '*') continue // comment
      // continued line, otherwise the last subline would have length 0
      if (sublines.length == 0) {
        prev = line
        break
      }
      hash += line
      // section declaration
      if (line[0] == '$') {
        name = line
        if (stack.length == 0) stack.push(obj)
        while (stack.length > 1) {
          warn(`sec: stacklevel ${stack.length}, should be 1`)
          stack.pop()
        }
        obj = stack[0][name] = {}
        // paragraph definition identifier
        pardef = 'def'
        if (name == '$SCHED') pardef = 'scan'
        continue
      }
      // paragraph declaration
      if (line.slice(0, pardef.length + 1) == pardef + ' ') {
        name = line.slice(pardef.length + 1)
        if (stack.length == 1) { stack.push(obj) }
        else warn(`par: stacklevel ${stack.length}, should be 1`)
        obj = obj[name] = {}
        continue
      }
      if (line == `end${pardef}`) {
        if (stack.length == 2) { obj = stack.pop() }
        else warn(`end${pardef}: stacklevel ${stack.length}, should be 2`)
        continue
      }
      // parameter declaration
      [k, ...v] = line.split('=').map(e => e.trim())
      if (v.length != 1) {
        warn(`invalid parameter declaration: ${line}`)
        continue
      }
      if (!Object.prototype.hasOwnProperty.call(obj, k)) {
        obj[k] = v[0]
      } else {
        if (!Array.isArray(obj[k])) obj[k] = [obj[k]]
        obj[k].push(v[0])
      }
    }
  }
  // last paragraph was not terminated, it may be incomplete
  if (stack.length > 1) {
    console.log("last paragraph unterminated, drop it:", filename, name)
    delete obj[name]
  }
  // incomplete input will leave the stack empty
  if (stack.length > 0) obj = stack[0]
  // checksum
  obj['checksum'] = crc32(hash)
  return obj
}

// Replace references with the contents of the referenced objects
const vexexpand = function VexobjectReferenceExpansion(vex, o = null) {
  if (o === null) o = vex
  Object.entries(o).forEach(([k, v]) => {
    if (typeof(v) == 'object') return vexexpand(vex, v)
    if (typeof(v) != 'string') return
    if (k.slice(0, 4) != 'ref ') return
    const ref = k.slice(4)
    if (!Object.prototype.hasOwnProperty.call(vex, ref)) return
    if (!Object.prototype.hasOwnProperty.call(vex[ref], v)) return
    if (!typeof(vex[ref][v]) == 'object') return

    // replace reference with the contents of the object it refers to
    delete o[k]
    Object.assign(o, vex[ref][v])
  })
  return o
}

// VexobjectValueInterpretation does three things:
// 1. It splits multi-value strings and inserts each of its substrings as
// key-value pair using their key names as they are defined in the vexfile
// reference manual v1.5b.
// 2. It casts some of the string-typed values to non-string types where
// possible, such as times and integers.
// 3. It adds a number of data fields that are useful to have, such as scan
// stop time
const vexinterpret = function VexobjectValueInterpretation(vex) {
  let keys

  // vexfile key-value values are either type string or string-array.
  // These helper functions facilitate mapping to both possible values.
  const map2 = function mapStringOrArrayOfStrings(v, f) {
    if (typeof(v) == 'string') return f(v)
    if (Array.isArray(v)) return v.map(vv => f(vv))
    return v
  }
  const reduce2 = function reduceStringOrArrayOfStrings(v, f, init) {
    if (typeof(v) == 'string') return f(init, v)
    if (Array.isArray(v)) return v.reduce((a, vv) => f(a, vv), init)
    return init
  }

  // $SCHED
  const schedkeys = keys = '$STATIONkey datastart datastop startpos pass pointsectr tapedrives'.split(' ')
  Object.values(vex['$SCHED']).forEach(scan => {
    // helper key: js time at which this event happens
    scan.start = vex2time(scan.start)
    // replace station string value with object
    if (typeof(scan.station) == 'string') scan.station = [scan.station]
    scan.station = map2(scan.station, function (v) {
      let station = v.split(':').reduce((o, e, i) => ({...o, [keys[i]]: e.trim()}), {})
      // helper keys: js time at which these events happen
      'datastart datastop'.split(' ').forEach(k => {
        const [v, u] = station[k].split(' ')
        if (u != 'sec') console.log(`ERROR: scan unit is not 'sec': ${vex.filename}: ${u}`)
        station[k] = parseInt(v)
      })
      return station
    })
    // helper key: js time at which this event happens
    scan.stop = scan.start + 1000*reduce2(scan.station, (a, e) => e.datastop>a?e.datastop:a, 0)
  })

  // $FREQ
  const freqkeys = keys = 'Band_ID Sky_freq_at_0Hz_BBC Net_SB Chan_BW Chan_ID BBC_ID PhaseCal_ID'.split(' ')
  Object.values(vex['$FREQ']).forEach(par => {
    // replace chan_def string value with object
    par.chan_def = map2(par.chan_def, function (v) {
      let val = v.split(':').reduce((o, e, i) => ({...o, [keys[i]]: e.trim()}), {})
      return val
    })
  })

  // $GLOBAL
  vex.$GLOBAL.exper_nominal_start = vex2time(vex.$GLOBAL.exper_nominal_start)
  vex.$GLOBAL.exper_nominal_stop = vex2time(vex.$GLOBAL.exper_nominal_stop)

  // $IF
  const ifkeys = keys = 'IF_ID Physical_Pol_Name Total_LO Net_SB PhaseCal_freq_spacing PCal_base_freq'.split(' ')
  Object.values(vex['$IF']).forEach(par => {
    // replace if_def string value with object
    par.if_def = map2(par.if_def, function (v) {
      let val = v.split(':').reduce((o, e, i) => ({...o, [keys[i]]: e.trim()}), {})
      return val
    })
  })

  return vex
}

// express js date in vexfile-time format
const date2vex = function formatJSDateAsVextime(date) {
  const year = date.getUTCFullYear()
  let doy = (Date.UTC(year, date.getUTCMonth(), date.getUTCDate()) - Date.UTC(date.getUTCFullYear(), 0, 0)) / (24 * 60 * 60 * 1000)
  doy = ('00' + doy).slice(-3)
  const hour = ('0' + date.getUTCHours()).slice(-2)
  const minute = ('0' + date.getUTCMinutes()).slice(-2)
  const second = ('0' + date.getUTCSeconds()).slice(-2)
  return `${year}y${doy}d${hour}h${minute}m${second}s`
}
const time2vex = function formatJSTimeAsVextime(time) {
  return date2vex(new Date(time))
}

// calculate js time (unix time * 1000) from vexfile-time
const vex2time = function parseVextimeIntoJSTime(vextime) {
  if (typeof(vextime) != 'string') return null
  const words = vextime.split(/[ydhms]/).map(x => parseInt(x))
  return Date.UTC(words[0], 0, words[1], words[2], words[3], words[4], 0)
}
const vex2date = function parseVextimeIntoJSDate(vextime) {
  return new Date(vex2time(vextime))
}

// testsuite
const vextime_testsuite = [
  '2020-02-29T20:14:43Z',
  '2020-02-28T13:14:43Z',
  '2020-03-01T01:14:43Z',
  '2020-01-01T00:00:00Z',
  '2020-12-31T23:59:59Z',
  ].every(d => {
    // don't set milliseconds!
    const date = new Date(d)
    const time = 1*date
    const identities = {
      vex: [date2vex(date), time2vex(time)],
      date: [time, 1*vex2date(date2vex(date))],
      time: [time, vex2time(time2vex(time))],
    }
    const ok = Object.entries(identities).reduce((a, e) => {
      const pass = e[1][0] == e[1][1]
      if (!pass) console.log(`error: ${e[0]}: ${e[1][0]} != ${e[1][1]}`)
      return a && pass
    }, true)
    if (!ok) console.log(`vextime_testsuite failed: ${date}`)
    return ok
  })


/*
http://www.webtoolkit.info/javascript-crc32.html
https://stackoverflow.com/questions/18638900/javascript-crc32
*/
const table =
  "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D"
  .split(' ').map(s => parseInt(s, 16))

const crc32 = function (str) {
    let crc = -1
    for(let i=0, iTop=str.length; i<iTop; i++) {
        crc = (crc >>> 8)^table[(crc^str.charCodeAt(i)) & 0xFF]
    }
    return (crc^(-1)) >>> 0
}
