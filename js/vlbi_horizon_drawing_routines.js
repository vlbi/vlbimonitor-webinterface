function makeHorizonMaps() {
	
	$.each(datastore.metadata,function(observatoryname, observatorydata) {
		// metadata can be undefined in which case we simply skip to the next:
		if(observatorydata==undefined) return true; 
		if(!datastore.visibility.observatories[observatoryname]) return true;
		if($("#horizon_"+observatoryname).length==0) return true;
		
		// if(getURLParameter("observatory")!=undefined && getURLParameter("observatory")!=observatoryname) return true;

		
		// calculate lookat coordinates that will result in a horizontal horizon (looking at 0,0 in AzAlt)
		// var geopos = [lat, lon];
		// var geopos = splitRaDec(contexts[observatoryname].get("observatory_coordinates"));
		var geopos = splitRaDec(datastore.data[observatoryname]["observatory_coordinates"].value);
		var lat = geopos[0];
		var lon = geopos[1];
		// on the northern hemisphere look south and vice versa
		lookat = Celestial.getPoint(Celestial.horizontal.inverse(new Date(), [0, geopos[0]<0?0:180], geopos), "equatorial");  // last argument must match config.transform below!!
		lookat[2] = lat<0?180:0;


		// this function was copied from d3.geo.projections, unfortunately it is not public in that module
		function parallel1Projection(projectAt) {
		    var φ0 = 0, m = d3.geo.projectionMutator(projectAt), p = m(φ0);
		    p.parallel = function(_) {
		      if (!arguments.length) return φ0 / Math.PI * 180;
		      return m(φ0 = _ * Math.PI / 180);
		    };
		    return p;
		}

		// this is an almost verbatim copy of the cylindricalEqualArea projection except for a factor 2
		function cylindricalEqualAreaStretched(φ0) {
		    var cosφ0 = Math.cos(φ0);
		    function forward(λ, φ) {
		      return [ 2 * λ * cosφ0, Math.sin(φ) / cosφ0 ];
		    }
		    forward.invert = function(x, y) {
		      return [ x / (2*cosφ0), Math.asin(y * cosφ0) ];
		    };
		    return forward;
		}

		// we add the stretched tranform to the projections library on the fly
		(d3.geo.cylindricalEqualAreaStretched = function() {
			return parallel1Projection(cylindricalEqualAreaStretched);
		}).raw = cylindricalEqualAreaStretched;


		var config = { 
			width: 0/*344*/,           // Default width, 0 = full parent element width; 
													// height is determined by projection
			projection: "cylindricalEqualAreaStretched",    // Map projection used: see above
			transform: "equatorial", // Coordinate transformation: equatorial (default),
															 // ecliptic, galactic, supergalactic
			center: lookat,       // Initial center coordinates in set transform
													// [longitude, latitude, orientation] all in degrees 
													// null = default center [0,0,0]
			orientationfixed: true,  // Keep orientation angle the same as center[2]
			adaptable: true,    // Sizes are increased with higher zoom-levels
			interactive: false,  // Enable zooming and rotation with mousewheel and dragging
			form: false,         // Display form for interactive settings
			location: false,    // Display location settings (no center setting on form)
			controls: false,     // Display zoom controls
			container: "horizon_"+observatoryname,   // ID of parent element, e.g. div, null = html-body
			datapath: "/js/data/",  // Path/URL to data files, empty = subfolder 'data'
			stars: {
				show: true,    // Show stars
				limit: 4,      // Show only stars brighter than limit magnitude
				colors: true,  // Show stars in spectral colors, if not use default color
				style: { fill: "#ffffff", opacity: 1 }, // Style for stars
				names: false,   // Show star names (Bayer, Flamsteed, Variable star, Gliese, 
										 //     whichever applies first in that order)
				proper: false, // Show proper name (if present, above name otherwise)
				desig: false,  // Show all names, including Draper and Hipparcos
				namelimit: 10,  // Show only names/designations for stars brighter than namelimit
				namestyle: { fill: "#ddddbb", font: "11px Georgia, Times, 'Times Roman', serif", 
										 align: "left", baseline: "top" },  // Style for star names
				size: 3,       // Maximum size (radius) of star circle in pixels
				data: 'stars.3.5.json' // Data source for stellar data, 
														 // number indicates limit magnitude
			},
			dsos: {
				show: true,    // Show Deep Space Objects 
				limit: 1,      // Show only DSOs brighter than limit magnitude
				names: true,   // Show DSO names
				desig: true,   // Show short DSO names
				namestyle: { fill: "#cccccc", font: "11px Helvetica, Arial, serif", 
										 align: "left", baseline: "top" }, // Style for DSO names
				namelimit: 6,  // Show only names for DSOs brighter than namelimit
				data: 'dsos.bright.json', // Data source for DSOs, 
																	// opt. number indicates limit magnitude
				symbols: {  //DSO symbol styles, 'stroke'-parameter present = outline
					gg: {shape: "circle", fill: "#ff0000"},          // Galaxy cluster
					g:  {shape: "ellipse", fill: "#ff0000"},         // Generic galaxy
					s:  {shape: "ellipse", fill: "#ff0000"},         // Spiral galaxy
					s0: {shape: "ellipse", fill: "#ff0000"},         // Lenticular galaxy
					sd: {shape: "ellipse", fill: "#ff0000"},         // Dwarf galaxy
					e:  {shape: "ellipse", fill: "#ff0000"},         // Elliptical galaxy
					i:  {shape: "ellipse", fill: "#ff0000"},         // Irregular galaxy
					oc: {shape: "circle", fill: "#ffcc00", 
							 stroke: "#ffcc00", width: 1.5},             // Open cluster
					gc: {shape: "circle", fill: "#ff9900"},          // Globular cluster
					en: {shape: "square", fill: "#ff00cc"},          // Emission nebula
					bn: {shape: "square", fill: "#ff00cc", 
							 stroke: "#ff00cc", width: 2},               // Generic bright nebula
					sfr:{shape: "square", fill: "#cc00ff", 
							 stroke: "#cc00ff", width: 2},               // Star forming region
					rn: {shape: "square", fill: "#00ooff"},          // Reflection nebula
					pn: {shape: "diamond", fill: "#00cccc"},         // Planetary nebula 
					snr:{shape: "diamond", fill: "#ff00cc"},         // Supernova remnant
					dn: {shape: "square", fill: "#999999", 
							 stroke: "#999999", width: 2},               // Dark nebula grey
					pos:{shape: "diamond", fill: "#cccccc",
							 stroke: "#cccccc", width: 2},              // Generic marker
					calibrator: { shape: "diamond", fill: "#cccccc",
					     stroke: "#cccccc", width: 2},
					source: { shape: "diamond", fill: "#00ff00",
					     stroke: "#00ff00", width: 2}
				}
			},
			constellations: {
				show: false,    // Show constellations 
				names: true,   // Show constellation names 
				desig: true,   // Show short constellation names (3 letter designations)
				namestyle: { fill:"#cccc99", font: "12px Helvetica, Arial, sans-serif", 
										 align: "center", baseline: "middle" }, // Style for constellations
				lines: true,   // Show constellation lines, style below
				linestyle: { stroke: "#cccccc", width: 1, opacity: 0.6 }, 
				bounds: false, // Show constellation boundaries, style below
				boundstyle: { stroke: "#cccc00", width: 0.5, opacity: 0.8, dash: [2, 4] }
			},
			mw: {
				file: "mw.t1.0.min.json",
				show: true,     // Show Milky Way as filled multi-polygon outlines 
				style: { fill: "#ffffff", opacity: 0.15 }  // Style for MW
			},
			lines: {  // Display & styles for graticule & some planes
				graticule: { show: true, stroke: "#cccccc", width: 0.6, opacity: 0.3 },   
				equatorial: { show: true, stroke: "#aaaaaa", width: 1.3, opacity: 0.5 },  
				ecliptic: { show: true, stroke: "#66cc66", width: 1.3, opacity: 0.7 },     
				galactic: { show: true, stroke: "#ff7700", width: 1.3, opacity: 0.7 },    
				supergalactic: { show: false, stroke: "#cc66cc", width: 1.3, opacity: 0.7 }
			},
			background: {        // Background style
				fill: "#000000",   // Area fill
				opacity: 1, 
				stroke: "#000000", // Outline
				width: 1 
			}, 
			horizon: {  //Show horizon marker, if location is set and map projection is all-sky
				show: true, 
				width: 1.0, 
				stroke: "rgba(255,0,0,1)", // Line
				fill: "rgba(0,0,0,0.01)", // Area below horizon
				opacity: 0.5
			}
		};

		var horizonmap = makeCelestial();
		// add horizon
		horizonmap.add({
		    type: "line", 
		    callback: function() { 
		        // construct  geojson object for rendering:
		        var realhorizon = {
		            "type": "Polygon",
		            "coordinates": [$.map(getHorizonMap(observatoryname).reverse(),function(azAlt) {
		                
		                var tmp = horizontal2equatorial(
		                    azAlt.az    *radians,
		                    azAlt.alt   *radians,
		                    lat         *radians,
		                    lon         *radians
		                );
		                return [[   // double array required because polygons are described by lists of lists of points (each list being connected itself but not to the other lists) (and map flattens once when given an array...)
		                    tmp[0]/radians,
		                    tmp[1]/radians
		                ]];
		            })]
		        };

		        
		        // draw the actual thing:
		        horizonmap.container.append("path")
		            .datum(realhorizon)
		            .attr("class","realhorizon");
		        horizonmap.container.append("path")
		            .attr("class","idealhorizon");

		    },
		    redraw: function() {
		        // set style (and start path construction as a side-effect)     
		        horizonmap.setStyle({
		            show: true, 
		            //stroke: "#ff0000", // Line
		            stroke: "rgba(255,0,0,0.75)", // Line
		            width: 1.0, 
		            fill: "rgba(0,0,0,0)" // Area below horizon
		            //fill: "#000000", // Area below horizon
		            //opacity: 0.75
		        });

		        // set d path of the element (horizonmap.map is a function that knows to look at the currently bound data. as a side-effect this adds lineto commands to the current context)
		        horizonmap.container.selectAll(".realhorizon")
		            //  data was set once in the callback above and does not need to change
		            // .datum(realhorizon)
		            .attr("d", horizonmap.map);

		        // stroke and fill the paths that have been constructed
		        horizonmap.context.fill();    
		        horizonmap.context.stroke();    

		        // draw the astronomical horizon as well: (mmostly stolen from celestials horizon functionality)
		        var circle = d3.geo.circle().angle([90]);
		        var zenith = Celestial.getPoint(horizonmap.horizontal.inverse(new Date(), [90, 0], [lat,lon]), horizonmap.settings().transform);
		        zenith[2] = 0;
		        circle.origin(zenith);
		        horizonmap.setStyle({
		            show: true, 
		            stroke: "#ff0000", // Line
		            width: 1.0, 
		            opacity: 0.5
		        });
		        
		        horizonmap.container.selectAll(".idealhorizon").datum(circle).attr("d", horizonmap.map);  
		        horizonmap.context.stroke();    
		    }

		});
		
		addPointing(horizonmap,config,[observatoryname]);
		addSchedule(horizonmap,config);
		
		horizonmap.display(config);
		// save the object for future manipulations
		$("#horizon_"+observatoryname).data("celestial",horizonmap);
		$("#horizon_"+observatoryname).data("celestialconfig",config);
	});

	updateHorizons();
}




function updateSchedulesAndPointers() {
	// console.log("updating schedules and pointers")
	$.each(datastore.metadata,function(observatoryname, observatorydata) {
		if(observatorydata==undefined) return true; 
		if(!datastore.visibility.observatories[observatoryname]) return true;
		if($("#horizon_"+observatoryname).length==0) return true;
		// if(getURLParameter("observatory")!=undefined && getURLParameter("observatory")!=observatoryname) return true;

		var map  = $("#horizon_"+observatoryname).data("celestial");
		var conf = $("#horizon_"+observatoryname).data("celestialconfig");
		updatePointingMarkers(map, conf);
		updateScheduleMarkers(map, conf);
		map.redraw();
		$("#horizon_"+observatoryname+" .loader").hide();
	});
}





/* the updateHorizons function can be called to immediately update the horizons (e.g. when the schedule has changed. It will reschedule itself because the horizon needs updating since it moves through the galactic plane 15 degrees per hour.)  */
var horizonUpdateLoop = null;
function updateHorizons() {
	// cancel any running update timer to make sure this is not executed twice
	clearTimeout(horizonUpdateLoop);
	

	each_interleaved(datastore.metadata,100,function(observatoryname, observatorydata) {
		// metadata can be undefined in which case we simply skip to the next:
		if(observatorydata==undefined) return true; 
		if(!datastore.visibility.observatories[observatoryname]) return true;
		if($("#horizon_"+observatoryname).length==0) return true;
		// if(getURLParameter("observatory")!=undefined && getURLParameter("observatory")!=observatoryname) return true; 

		// split lan and lon
		var geopos = splitRaDec(datastore.data[observatoryname]["observatory_coordinates"].value);

		// get the Celestial object:
		var h = $("#horizon_"+observatoryname).data("celestial");

		// construct  geojson object for rendering:
		var realhorizon = {
		    "type": "Polygon",
		    "coordinates": [$.map(getHorizonMap(observatoryname),function(azAlt) {
		        
		        var tmp = horizontal2equatorial(
		            azAlt.az    *radians,
		            azAlt.alt   *radians,
		            geopos[0]   *radians,
		            geopos[1]   *radians
		        );
		        return [[   // double array required because polygons are described by lists of lists of points (each list being connected itself but not to the other lists) (and map flattens once when given an array...)
		            tmp[0]/radians,
		            tmp[1]/radians
		        ]];
		    })]
		};

		h.container.selectAll(".realhorizon").datum(realhorizon);

		lookat = Celestial.getPoint(Celestial.horizontal.inverse(new Date(), [0, geopos[0]<0?0:180], geopos), "equatorial");  // last argument must match config.transform below!!
		lookat[2] = geopos[0]<0?180:0;
		
		h.rotate({center:lookat, horizon:h.settings().horizon});
		// h.redraw();
	},function() { // this is called by each_interleaved when it reached the end
		// schedule the next update
		horizonUpdateLoop = setTimeout(updateHorizons, 60000);
	});
}


