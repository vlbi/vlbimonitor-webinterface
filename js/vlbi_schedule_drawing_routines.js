

// renderCrosshair draws a crosshair (either pointing or target location) on a html canvas context
function renderCrosshair(d,pt,r,ctx) {

    switch(d.properties.symbol) {
        case "obs":
            ctx.arc(pt[0], pt[1], r, 0, 2 * Math.PI);
            break;
        case "target":
            ctx.moveTo(pt[0],pt[1]+1*r);
            ctx.lineTo(pt[0],pt[1]+2*r);
            
            ctx.moveTo(pt[0],pt[1]-1*r);
            ctx.lineTo(pt[0],pt[1]-2*r);
            
            ctx.moveTo(pt[0]+1*r,pt[1]);
            ctx.lineTo(pt[0]+2*r,pt[1]);
            
            ctx.moveTo(pt[0]-1*r,pt[1]);
            ctx.lineTo(pt[0]-2*r,pt[1]);
            break;
    }


    // for certain names we add extra symbols:
    if(d.properties.symbol=="obs" && d.properties.name=="APEX") {
        // square
        ctx.moveTo(pt[0]+1.5*r,pt[1]+1.5*r);
        ctx.lineTo(pt[0]+1.5*r,pt[1]-1.5*r);
        ctx.lineTo(pt[0]-1.5*r,pt[1]-1.5*r);
        ctx.lineTo(pt[0]-1.5*r,pt[1]+1.5*r);
        ctx.lineTo(pt[0]+1.5*r,pt[1]+1.5*r);
    }
    if(d.properties.symbol=="obs" && d.properties.name=="JCMT") {
        // diamond
        ctx.moveTo(pt[0]+2*r,pt[1]);
        ctx.lineTo(pt[0],pt[1]-2*r);
        ctx.lineTo(pt[0]-2*r,pt[1]);
        ctx.lineTo(pt[0],pt[1]+2*r);
        ctx.lineTo(pt[0]+2*r,pt[1]);
    }
    if(d.properties.symbol=="obs" && d.properties.name=="LMT") {
        // upside down triangle
        ctx.moveTo(pt[0]+2*r*Math.cos( 3*Math.PI/6),pt[1]+2*r*Math.sin( 3*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 7*Math.PI/6),pt[1]+2*r*Math.sin( 7*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos(11*Math.PI/6),pt[1]+2*r*Math.sin(11*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 3*Math.PI/6),pt[1]+2*r*Math.sin( 3*Math.PI/6));
        // ctx.lineTo(pt[0]+2*r*Math.cos(-Math.PI/3),pt[1]+2*r*Math.sin(-Math.PI/3));
    }
    if(d.properties.symbol=="obs" && d.properties.name=="PdBURE") {
        // triangle
        ctx.moveTo(pt[0]+2*r*Math.cos( 1*Math.PI/6),pt[1]+2*r*Math.sin( 1*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 5*Math.PI/6),pt[1]+2*r*Math.sin( 5*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 9*Math.PI/6),pt[1]+2*r*Math.sin( 9*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 1*Math.PI/6),pt[1]+2*r*Math.sin( 1*Math.PI/6));
        // ctx.lineTo(pt[0]+2*r*Math.cos(-Math.PI/3),pt[1]+2*r*Math.sin(-Math.PI/3));
    }
    if(d.properties.symbol=="obs" && d.properties.name=="PICO") {
        // 3 lines
        ctx.moveTo(pt[0]+2*r*Math.cos( 0*Math.PI/6),pt[1]+2*r*Math.sin( 0*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 2*Math.PI/6),pt[1]+2*r*Math.sin( 2*Math.PI/6));
        ctx.moveTo(pt[0]+2*r*Math.cos( 4*Math.PI/6),pt[1]+2*r*Math.sin( 4*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos( 6*Math.PI/6),pt[1]+2*r*Math.sin( 6*Math.PI/6));
        ctx.moveTo(pt[0]+2*r*Math.cos( 8*Math.PI/6),pt[1]+2*r*Math.sin( 8*Math.PI/6));
        ctx.lineTo(pt[0]+2*r*Math.cos(10*Math.PI/6),pt[1]+2*r*Math.sin(10*Math.PI/6));
    }
    if(d.properties.symbol=="obs" && d.properties.name=="SMA") {
        // trapezoid
        ctx.moveTo(pt[0]+1.0*r,pt[1]+1.5*r);
        ctx.lineTo(pt[0]-1.0*r,pt[1]+1.5*r);
        ctx.lineTo(pt[0]-2.0*r,pt[1]-1.5*r);
        ctx.lineTo(pt[0]+2.0*r,pt[1]-1.5*r);
        ctx.lineTo(pt[0]+1.0*r,pt[1]+1.5*r);
    }
    if(d.properties.symbol=="obs" && d.properties.name=="SMTO") {
        // reverse trapezoid
        ctx.moveTo(pt[0]+2.0*r,pt[1]+1.5*r);
        ctx.lineTo(pt[0]-2.0*r,pt[1]+1.5*r);
        ctx.lineTo(pt[0]-1.0*r,pt[1]-1.5*r);
        ctx.lineTo(pt[0]+1.0*r,pt[1]-1.5*r);
        ctx.lineTo(pt[0]+2.0*r,pt[1]+1.5*r);
    }
    // other telescopes simply have the circle
    ctx.stroke();
    
    
}



/* helpers for angle conversions */
function ra2hours(ra) {
    let regex = /^(-)?(\d+)h(\d+)m(\d+(\.\d+)?)s$/;
    let matches = regex.exec(ra);
    if (matches == null) return 0;
    let sign    = matches[1]?-1:1;
    let hours   = parseInt(matches[2]);
    let minutes = parseInt(matches[3]);
    let seconds = parseFloat(matches[4]);
    return sign*(hours + minutes/60 + seconds/3600);
}
function dec2degrees(dec) {
    let regex = /^(-)?(\d+)d(\d+)'(\d+(\.\d+)?)"$/;
    let matches = regex.exec(dec);
    if (matches == null) return 0;
    let sign    = matches[1]?-1:1;
    let degrees = parseInt(matches[2]);
    let minutes = parseInt(matches[3]);
    let seconds = parseFloat(matches[4]);
    return sign*(degrees + minutes/60 + seconds/3600);
}



// addCrosshair does renderCrosshair, and additionally places the label next to it
function addCrosshair(Celestial, d) {
    if ( Celestial.clip(d.geometry.coordinates) ) {
        var pt = Celestial.mapProjection(d.geometry.coordinates);
        var r = 6;
        Celestial.setStyle(d.properties.style);
        renderCrosshair(d,pt,r,Celestial.context);

        // add the label     
        var hanchor, vanchor;
        var hoffset, voffset;
        switch(d.properties.name){
            case "APEX": // square
                hanchor="left";
                vanchor="bottom";
                hoffset=1.5*r;
                voffset=0;
                break;
            case "JCMT": // diamond
                hanchor="right";
                vanchor="top";
                hoffset=-1.5*r;
                voffset=0;
                break;
            case "LMT": // usd triangle
                hanchor="left";
                vanchor="top";
                hoffset=0.2*r;
                voffset=-1.5*r;
                break;
            case "PdBURE": // triangle
                hanchor="left";
                vanchor="bottom";
                hoffset=0.2*r;
                voffset=1.5*r;
                break;
            case "PICO": // 3 lines
                hanchor="right";
                vanchor="bottom";
                hoffset=-1.5*r;
                voffset=0.2*r;
                break;
            case "SMA":  // usd trapezoid
                hanchor="right";
                vanchor="bottom";
                hoffset=-0.2*r;
                voffset=1.5*r;
                break;
            case "SMTO":  // trapezoid
                hanchor="right";
                vanchor="top";
                hoffset=-0.2*r;
                voffset=-1.5*r;
                break;
            default:
                hanchor="left";
                vanchor="top";
                hoffset=r;
                voffset=r;
        }

        if(d.id=="ScanCurrent" || d.id=="ScanNext" || d.id=="ScanBoth" ) {
            hoffset=0.5*r;
            voffset=0.5*r;   
        }
        
        // Celestial.setTextStyle();
        var textstyle = { fill: "#ffffff", font: "bold 12px Roboto, sans-serif", align: hanchor, baseline: vanchor };
        textstyle = Object.assign(textstyle,d.properties.textstyle); // override with custom provided properties
        Celestial.setTextStyle(textstyle);
        Celestial.context.fillText(d.properties.name, pt[0]+hoffset, pt[1]-voffset);
    }
}




  

/* add symbol to the map showing where each telescope is pointing to right now */
function addPointing(Celestial, config, observatories) {

    // add observatories:
    Celestial.add({
        type: "line", // we actually add a crosshair marker but this allows us to not have a "file" attribute
        callback: function() { 
            // must have a callback
        },  
        redraw: function() {
            Celestial.container.selectAll(".pointer").each(function(d) {
                if(observatories==undefined || observatories.indexOf(d.properties.name)>=0) {
                    addCrosshair(Celestial, d);
                }
            });
        }
    });
}


// translate datastore coords to feature collection:
function makePointingFeatures() {
    var hours2degrees = function(radec) {radec[0]*=15;return radec;};
     var sourcedata = {
        "type": "FeatureCollection",
        "features": $.map(datastore.data,function(obsdata,obsname) {
            if(obsdata == undefined || obsdata["telescope_apparentRaDec"].value==undefined) {
                return null; // skip this in list
            }
            var res = {
                "type": "Feature",
                "id": obsname+"-pointer",
                "properties": {
                    "name": obsname,
                    "style": {stroke: "#ffffff", width: 1.5},
                    "symbol": "obs"
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": hours2degrees(splitRaDec(obsdata["telescope_apparentRaDec"].value))
                }
            };
            return res;
        })
    };

    return sourcedata;
}


/* new function that auto creates/removes pointers when required */
function updatePointingMarkers(Celestial, config) {
    // if(!observatories) {
    //     observatories = []; // means get all
    // }
         
    var features = makePointingFeatures();

    var selection = Celestial.container.selectAll(".pointer")
        .data(Celestial.getData(features, config.transform).features);

    selection.enter()
        .append("path")
        .attr("class","pointer");
    selection.exit().remove();
}




function makeScheduleFeatures() {
    return {
        "type": "FeatureCollection",
        "features": $.map(datastore.schedule.$SOURCE, function(d) {
            return {
                "type": "Feature",
                "id": d.source_name,
                "properties": {
                    "name": d.source_name,
                    "type": "source",
                    "mag": 0, // i.e. always visible
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        ra2hours(d.ra) * 15,
                        dec2degrees(d.dec)
                    ]
                }
            };
        })
    };
}


// add schedule objects and lines:
function addSchedule(Celestial,config) {
    Celestial.add({
        type: "line", // we actually add a crosshair marker but this allows us to not have a "file" attribute
        callback: function() { // and it must have a callback

        },
        redraw: function() {
            // redraw target markers:
            Celestial.container.selectAll(".target").each(function(d) {
                if(d) { // skip if there is no data
                    addCrosshair(Celestial, d);
                }
            });
        }
    });
}

function updateScheduleMarkers(Celestial,config) {
    //  find the current and next scan:
    var current = getCurrentScheduleItem();
    var {next, vex} = getNextScheduleItem();

    // create an empty featurecollection
    var scanmarkerdata = {
        "type": "FeatureCollection",
        "features": []
    };

    // add current scanner
    if(current && next && current.Source == next.Source) {
        var coords = [
            ra2hours(vex.$SOURCE[current.Source].ra) * 15,
            dec2degrees(vex.$SOURCE[current.Source].dec)
	];
        scanmarkerdata.features[0] = {
            "type": "Feature",
            "id": "ScanBoth",
            "properties": {
                "name": "current/next target",
                "style": {stroke: "#ffffff", width: 3},
                "textstyle": { fill: "#ffffff", align: "left", baseline: "bottom"  },
                "symbol": "target"
            },
            "geometry": {
                "type": "Point",
                "coordinates": coords
            }
        };
    } else {
        if(current) {
            var current_coords = [
                ra2hours(datastore.schedule.$SOURCE[current.Source].ra) * 15,
		dec2degrees(datastore.schedule.$SOURCE[current.Source].dec)
	    ];
            scanmarkerdata.features[0] = {
                "type": "Feature",
                "id": "ScanCurrent",
                "properties": {
                    "name": "target",
                    "style": {stroke: "#ffffff", width: 3},
                    "textstyle": { fill: "#ffffff", align: "left", baseline: "bottom" },
                    "symbol": "target"
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": current_coords
                }
            };
        }

        // add nextup scanner
        if(next) {
            var next_coords = [
		ra2hours(vex.$SOURCE[next.Source].ra) * 15,
		dec2degrees(vex.$SOURCE[next.Source].dec)
	    ];
            scanmarkerdata.features[1] = {
                "type": "Feature",
                "id": "ScanNext",
                "properties": {
                    "name": "next target",
                    "style": {stroke: "#00ff00", width: 3},
                    "textstyle": { fill: "#00ff00", align: "left", baseline: "bottom"  },
                    "symbol": "target"
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": next_coords                            
                }
            };
        }

    }

    

    // add sources from the vex schedules as DSO objects:
    var features = makeScheduleFeatures();
    var selection = Celestial.container.selectAll(".source")
        .data(Celestial.getData(features,config.transform).features,function(d) { // Celestial.getData converts between equatorial, celestial etc if required
            // this is the "key" function used to match existing elements to new data. d refers to the new data (or bound data for existing elements)
            // "this" may be used. It refers to the (pseudo) element or to the data if no element exists
            return d.id;
        });
    selection.enter()
        .append("path")
        .attr("class","dso source");
    

    selection = Celestial.container.selectAll(".target")
        .data(Celestial.getData(scanmarkerdata, config.transform).features);
    selection.enter()
        .append("path")
        .attr("class","target");
    selection.exit().remove();

}
