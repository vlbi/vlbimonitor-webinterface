
Object.defineProperty(Array.prototype,"date",{
	get: function () {return this[0]},
	set: function (val) {this[0] = val},
	enumerable: false
});


Object.defineProperty(Array.prototype,"value",{
	get: function () {return this[1]},
	set: function (val) {this[1] = val},
	enumerable: false
});


Object.defineProperty(Array.prototype,"valid",{
	get: function () {return this[2]},
	set: function (val) {this[2] = val},
	enumerable: false
});

Object.defineProperty(Array.prototype,"invalid",{
	get: function () {return this[3]},
	set: function (val) {this[3] = val},
	enumerable: false
});

var datastore = new function() {
	// data, metadata will be requested by ajax calls but we need the info later on so we save them in these vars:
	this.data = {};
	this.metadata = {};
	this.clients = {};
	this.schedules = {};
	this.messages = {};

	this.visibility = {
		schedules: {},
		observatories: {}
	};

	this.metadataDeferred = $.Deferred();

	// what should be updated:
	this.config = {
		updatedata: true,
		vexupdateinterval: 0,
		updateschedule: true,
		showcountdown: false,

		messageFacility: Cookies.get("messageFacility"),  // facility to post messages as:

		observatories: [],	// empty list means all
		fields: [],			// empty list means all
		idDataPrev: 0,  // unix time of previous data request

		// which form field to update when interval changes, must be a valid jquery selector
		intervalFormField: undefined
	}

	var self = this; // self is required because this may refer to the function calling context

	// private members:
	var callbacks = {data:[],metadata:[],schedule:[],all:[]};
	var updateinterval;
	var timer;
	// for each observatory we will create a "context" in wich valueFunctions/validFunctions are evaluated. see valueresolution.js
	var contexts = {};
	var lastScheduleUpdate = 0;
	var nextUpdate = 0;


	this.init = function() {
		// initially get the interval from cookie:
		updateinterval = Cookies.get("refreshrate") || "3";
		// show in form if it exits:
		$(document).ready(function(){
			$(self.config.intervalFormField).val(updateinterval);
		});


		// fetch visibility from cookie
		if(Cookies.get("observatoryvisibility")) {
			self.visibility.observatories = Cookies.getJSON("observatoryvisibility");
		} else {
			self.visibility.observatories = {};
		}
		// if in single observatory mode, filter:
		if(getURLParameter("observatory")!=undefined) {
			$.each(self.visibility.observatories, function(name,value){
				self.visibility.observatories[name] = false;
			});
			// this way we always set the url provided obs to true, even if it isn't in the cookie yet because this is somehow the first page the user visits
			self.visibility.observatories[getURLParameter("observatory")] = true;
		}

		// determine schedule visibility
		if(Cookies.get("schedulevisibility")) {
			self.visibility.schedules = Cookies.getJSON("schedulevisibility");
//DEBUG
//console.log('call fetchSchedule2')
//fetchSchedule2().then(ret => console.log(ret))
		} else {
			self.visibility.schedules = {};
		}


		if(self.config.showcountdown) {
			// <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
			//   <i class="material-icons">add</i>
			// </button>
			$(document).ready(function() {
				// remember when the first update will happen
				nextUpdate = new Date(new Date().getTime()+1000*updateinterval);

				// create the button
				var countdownarea = $("<div>").addClass("countdownarea");
				var countdownbutton = $("<button>");

				// main button
				countdownbutton.addClass("countdownbutton mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent mdl-shadow--4dp");
				var remaining = 0.5+(nextUpdate - new Date())/1000;//updateinterval - (new Date()-lastUpdate)/1000;
				countdownbutton.html(sprintTime(remaining));

				// handle mouse events:
				countdownbutton.on("mouseenter", function(e) {
					$(".countdownarea").addClass("expanded");
				});
				countdownarea.on("mouseleave", function(e) {
					$(".countdownarea").removeClass("expanded");
				});

				// handle touch events
				// countdownbutton.on({ 'touchstart' : function(e){
				//	 console.log("touched");
				// }});
				countdownbutton.on('touchend', function(e) {
					console.log("touched ended");
					$(".countdownarea").toggleClass("expanded");
					// the following may be confusing:
					// on touch devices the touchend may trigger a mouseenter, mouseleave or click depending on previous touch events in a way that emulates mouse events
					// when the user clicks refresh or changes the update frequency a mouseleave is generated for the countdownbutton
					// the following prevents a mousenter which would immediately reopen the thing if it was already open (and the above tobble closed it)
					if(!$(".countdownarea").hasClass("expanded")) {
						e.preventDefault();
					}

				});



				// refresh button
				var refreshbutton = $("<button>").html("<i class='material-icons'>refresh</i>");
				refreshbutton.addClass("refreshbutton mdl-button mdl-js-button mdl-button--fab mdl-shadow--4dp");
				refreshbutton.click(function() { self.update(); });


				// buttons for update interval
				var data = [1,3,10,60,180,600];
				var selection = d3.select(countdownarea.get(0)).selectAll(".timebutton").data(data);
				selection.enter().append("button")
					.attr("class","timebutton mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-shadow--4dp")
					.classed("active",function(d) {
						return d==updateinterval;
					})
					.html(function(d){return sprintTime(d)})
					.on('click',function(d) {
						self.setUpdateInterval(d);
						selection.classed("active",function(d1) {
							return d1==updateinterval;
						})
					})
					.each(function(d) {
						componentHandler.upgradeElement(this);
					});

				countdownarea.append(refreshbutton);
				countdownarea.append(countdownbutton);




				componentHandler.upgradeElement(countdownbutton.get(0));
				componentHandler.upgradeElement(refreshbutton.get(0));

				$("body").append(countdownarea);

				// schedule button text updates:
				setInterval(function() {
					var remaining = 0.5+(nextUpdate - new Date())/1000;//updateinterval - (new Date()-lastUpdate)/1000;
					countdownbutton.html(sprintTime(remaining));
				},1000);
			});
		}

		// the first update which will be triggered shortly, will update all dom that uses these visibility settings

		// fetch metadata once
		fetchMetadata().done(function() {
			// execute all callbacks asynchronously
			self.metadataDeferred.resolve();
			$.each(callbacks.metadata,function(i,c){setTimeout(c,1)});
		});
		// start updating
		self.update();
	}




	// methods to change the update interval
	this.setUpdateInterval = function(interval) {
		var prevvalue = updateinterval;
		updateinterval = parseInt(interval);
		if (updateinterval < prevvalue) {self.update()};
		Cookies.set("refreshrate",updateinterval);
		$(self.config.intervalFormField).val(updateinterval);
	}
	this.getUpdateInterval = function() {
		return updateinterval;
	}
	this.setFacility = function(facility) {
		// if(facility=="undefined") facility=undefined;
		self.config.messageFacility = facility;
		Cookies.set("messageFacility",self.config.messageFacility,{expires:999999999});

		// when the current page is the messaging page this will trigger a rerender of the warning
		if(typeof(updateTagWarnings)=="function") {
			updateTagWarnings();
		}
	}



	/* idea behind update callbacks:
	you can register data, metadata and schedule update callbacks individually.
	the onUpdate callback is called only once after all requests have finished.
	*/
	this.onUpdate = function(callback) {
		callbacks.all.push(callback);
	}
	this.onMetadata = function(callback) {
		callbacks.metadata.push(callback);
	}
	this.onData = function(callback) {
		callbacks.data.push(callback);
	}
	this.onSchedule = function(callback) {
		callbacks.schedule.push(callback);
	}


	this.update = function() {
		clearInterval(timer);
		// console.log("fetching updates")
		$("#updatespinner").show();

		var dataDeferred = $.Deferred();
		var scheduleDeferred = $.Deferred();

		// var deferreds = [metadataDeferred];
		if(self.config.updatedata) {
			dataDeferred = fetchData();
			dataDeferred.done(function() {
				$.each(callbacks.data,function(i,c){c()});
				// we don't resolve the dataDeferred yet because that requires valuefunctions to be executed
			});
			dataDeferred.fail(function() {
				showMessage("Server communication error");
			});
		} else {
			dataDeferred.resolve();
		}
		if(self.config.updateschedule && new Date()-lastScheduleUpdate > self.config.vexupdateinterval) {
			scheduleDeferred = fetchSchedule();
			scheduleDeferred.done(function() {
				lastScheduleUpdate = new Date();
				$.each(callbacks.schedule,function(i,c){c()});
			});
			scheduleDeferred.fail(function() {
				showMessage("Server communication error");
			});
		} else {
			scheduleDeferred.resolve();
		}

		// when all are finished update value/validFunctions and call the all callbacks
		// $.when.apply($,deferreds) returns a new jquery deferred object that triggers done when all of them are done (or failed)
		$.when(scheduleDeferred, dataDeferred, self.metadataDeferred).done(function() {
			resolveValuesAndValidities();
			$.each(callbacks.all,function(i,c){c()});
			$("#updatespinner").hide();
		});

		timer = setTimeout(self.update, 1000*updateinterval);
		nextUpdate = new Date(new Date().getTime()+1000*updateinterval);
	}




	function resolveValuesAndValidities() {

		$.each(self.metadata, function(observatoryname, observatorymetadata) {
			// console.log("  resolve values and validFunctions for "+observatoryname);
			// create the context (declare a var for each known variable etc)
			if(!(contexts[observatoryname])) {
				// for the next step (putting valueFunctions and validFunctions in the context) a list of known variablenames is required:
				var variablenames = [];
				$.each(self.metadata, function(observatoryname, observatorymetadata) {
					$.each(observatorymetadata, function(variable, vardata) {
						if(variablenames.indexOf(variable)<0){
							variablenames.push(variable);
						}
					});
				});
				// make the new context
				contexts[observatoryname] = makeEvalContext(variablenames);
				// fill it with the functions from the metadata
				contexts[observatoryname].initEvalContext(observatorymetadata);
			}

			// update with (non-function) data
			contexts[observatoryname].updateEvalContext(self.data[observatoryname]);

			// use empty object if there is no data
			if(!self.data[observatoryname]) {
				self.data[observatoryname] = {};
			}

			// update schedule in contexts
			var {nextScan, vex} = getNextScheduleItem()
			contexts[observatoryname].set('schedule', {
				nextScan,
				currentScan: getCurrentScheduleItem(),
				currentExperiment: getCurrentExperiment(),
			});
			contexts[observatoryname].set('observatoryname', "\""+observatoryname+"\"");

			// execute all functions:
			$.each(observatorymetadata, function(variablename, variablemetadata) {
				// console.log("for variable "+variablename + " in "+observatoryname)

				// add some empty data if it is missing
				if( ! self.data[observatoryname][variablename]) {
					self.data[observatoryname][variablename] = [];
				}
				// default time is now
				if (self.data[observatoryname][variablename].date == undefined) {
					self.data[observatoryname][variablename].date = (new Date).getTime()/1000;
				}
				// default value is undefined
				// which is already the default by not setting it at all
				// default  valid is true
				if (self.data[observatoryname][variablename].valid == undefined) {
					self.data[observatoryname][variablename].valid = true;
				}

				// if present execute valueFunction
				if("valueFunction" in variablemetadata) {
					// console.log("executing valueFunction for "+variablename+" in "+observatoryname)
					self.data[observatoryname][variablename].value = contexts[observatoryname].get(variablename);
				}

				// Note: things can have a validfunction even without data
				// this may happen, for instance, with aggregated category headers
				// their validity summarizes the validity of the whole category

				if("validFunction" in variablemetadata) {
					// whatever happens next, the default validity is true
					// todo a more advanced default could be to look at all child-fields
					// we may need to create an empty datapoint if no value exists:
					// if (!self.data[observatoryname][variablename]) {
					//	self.data[observatoryname][variablename] = [(new Date).getTime()/1000, undefined, true];
					// }
					// self.data[observatoryname][variablename].valid = true;

					// let "x" be the current value
					// self.data already contains the new value, either from data or from function at this point
					// console.log("  setting x to "+self.data[observatoryname][variablename].value);
					// TODO: children need to be set in each invocation rather than once
					// before recursively calling validfunctions...
					// the same probably holds for x
					// var children = $.map(observatorymetadata, function(i, childname) {
					//	if(childname.startsWith(variablename) && childname != variablename && variablename.slice(childname.length+1).indexOf(":")<0) {
					//		return childname;
					//	} else {
					//		return undefined; // skip this variable
					//	}
					// });
					// contexts[observatoryname].set("children", children);

					// attempt to execute valid function
					// console.log("  executing validFunction for "+variablename+" in "+observatoryname)
					try {
						self.data[observatoryname][variablename].valid = contexts[observatoryname].get(variablename+"_valid");

					}catch(e) {
						console.warn("execution of validFunc for \""+variablename+"\" in \""+observatoryname+"\" ran into an error: "+e);
						console.warn("the raw function text is: \""+variablemetadata.validFunction+"\"");
						console.warn(e);
					}
					try {
						self.data[observatoryname][variablename].invalid = contexts[observatoryname].get(variablename+"_invalid");
					} catch(e) {
					}

					// self.data[observatoryname][variablename].valid = contexts[observatoryname].get(variablename+"_valid");
				// } else {

				}
			});
		});

	}





	/* perform an xhr json request for data. After data is received it is saved and value/valid resolution is performed.
	For the latter it waits for metadata to have also arrived (even though the requests are fired asynchonously).
	This method immediately returns a deferred object that resolves after valueresolution is complete. */
	function fetchData() {

		var request = {
			jsonrpc: '2.0',
			method: 'getSnapshots',
			auth: {sessionId: Cookies.get('sessionid')},
			params: {idReference: self.config.idDataPrev, observatories: $.map(self.visibility.observatories,function(visible,observatoryname){return visible?observatoryname:undefined}), fields: self.config.fields}
		}

		var deferred = $.post("/rpc", JSON.stringify(request), function(response, status) {
			// console.log("data received");
			if (response.error) {
				showMessage(response.error.message,0,"Login again",function(){window.location.replace("/login.html")});
			}

			// The time at which the current request was received is the ID for the next request
			self.config.idDataPrev = response.recvtime

			// check what time it is on the server
			var timediff = new Date(response.sendtime * 1000) - new Date();

			if(timediff<0) timediff = - timediff;

			// console.log(timediff/1000)
			if(timediff/1000 < 600) {
				$("#timedifferror").hide();
			} else {
				$("#timedifferror span.notice").html("Your system clock appear to be off by "+sprintTime(timediff/1000,true)+". Since many visuals on this page are rendered using your brower's system time they may not be correct!");
				$("#timedifferror").show();
			}




			// save the data
			$.each(response.results, function(observatoryname, observatorydata) {
				self.data[observatoryname] = observatorydata.data;
				self.clients[observatoryname] = observatorydata.clients;
				self.messages[observatoryname] = observatorydata.messages;
			});


		}, "json");

		return deferred;
	};



	function fetchSchedule() {
		var requestSchedules = {};
		// console.log(datastore.visibility.schedules);
		// console.log(self.schedules);

		$.each(self.schedules, function(filename,data) {
			if(datastore.visibility.schedules[filename]) { // undefined would mean not visible in this case
				requestSchedules[filename] = data.MTime;
			}
		});
		// the first time that schedules are requested we need to populate it from the cookie based list of filenames since self.schedules is empty
		$.each(datastore.visibility.schedules, function(filename,visible) {
			if(visible) {
				requestSchedules[filename] = "";
			}
		});

		// console.log(requestSchedules)

		var request = {
			jsonrpc: '2.0',
			method: 'getSchedule',
			params: {"files": requestSchedules},
			auth: {sessionId: Cookies.get('sessionid')}
		}

		var deferred = $.post("/rpc",JSON.stringify(request),function(response,status) {
			if (response.error) {
				showMessage(response.error.message,0,"Login again",function(){window.location.replace("/login.html")});
			}
			// save the data
			$.each(response.results,function(filename,data) {
				// process newly added schedule files:
				if(!self.schedules[filename]) {
					// add data
					self.schedules[filename] = data;
					// make visible by default
					if(self.visibility.schedules[filename] == undefined) {
						self.visibility.schedules[filename] = !!data.ScheduleData; // if we receive data, then make it visible, double negation is cast to boolean
					} // if already defined then stick to what it was before
				};
				// process changed schedules:
				if(data.ScheduleData != null) {// only overwrite if there is actually data
					self.schedules[filename] = data;
					// self.schedules[filename].MTime = data.MTime;
					// self.schedules[filename].ScheduleName = data.ScheduleName;
					// self.schedules[filename].ScheduleData = data.ScheduleData;
				}
			});

			// remove schedules that aren't on the server anymore
			$.each(self.schedules,function(filename,data) {
				if(!(filename in response.results)) {
					// console.log("removing "+filename)
					delete self.schedules[filename];
					delete self.visibility.schedules[filename];
				}
			});

		},"json");
		// again return the deferred object
		return deferred;
	};

//
//DEBUG
//	function fetchSchedule2() {
//		const dirname = '/static/schedules/'
//
//		const promises = Object.keys(self.visibility.schedules)
//			.reduce((a, e) => {
//				// not selected
//				if (!datastore.visibility.schedules[e]) return a
//				// remove old path from filename
//				const filename = e.replace('/home/vlbimon/schedules/', '')
//				const promise = $.get(dirname + filename)
//					.then(ret => vexparser(ret, filename))
//					.fail(err => {console.log('error:', filename, err)})
//				return [...a, promise]
//			}, [])
//
//		return Promise.all(promises)
//	}
//DEBUG
//
//
	function fetchMetadata() {

		var deferred = $.getJSON("/static/masterlist.json", function(response, status) {
			
			// cookie visibility is stored in the cookie and handled separately from the current page visibility
			// since the current page may be in 'single observatory mode' (by setting ?observatory= url param)
			var cookievisibility = Cookies.getJSON("observatoryvisibility");
			if(cookievisibility==undefined) cookievisibility = {};
			
			window.defaultmetadata = response["default"];
			$.each(response, function(observatoryname, observatorydata) {
				if (observatoryname == "default") {
					return; // skip it, it's not a real observatory
				}
				
				if(cookievisibility[observatoryname]==undefined) {
					// new observatories default to visible
					cookievisibility[observatoryname] = true;
					// if the current page is not single-observatory mode, then add it to this page as well
					if(getURLParameter("observatory")==undefined) {
						self.visibility.observatories[observatoryname] = true;
					}
				}
				self.metadata[observatoryname] = JSON.parse(JSON.stringify(defaultmetadata)); // this is the cheapest operation that clones the map
				$.each(observatorydata, function(fieldname, fielddata) {
					Object.assign(self.metadata[observatoryname][fieldname], fielddata);
				});
				// self.metadata[observatoryname] = observatorydata;//Object.assign(observatorydata, defaultmetadata);
			});
			Cookies.set("observatoryvisibility",cookievisibility);
		});

		// again return the deferred object
		return deferred;
	};
}
// vim: noet
