const SCHEDULE_DIRNAME = '/static/schedules/trigger/';

function parameter(date, value) {
	this.value = value;
	this.valid = undefined;
	this.date = date;
	this.reason = [];
}

parameter.prototype.toString = function () {
	return this.value.toString();
}

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

const getSchedule = function ajaxGetSchedule(filename) {
	return $.get(SCHEDULE_DIRNAME + filename)
		.then(ret => vexparser(ret, filename))
		.then(ret => vexexpand(ret))
		.then(ret => vexinterpret(ret))
		.fail(err => {console.log('error:', filename, err)});
}

const getSchedules = schedules => Promise.all(schedules.map(e => getSchedule(e.filename)));


// Save the previous, current, and next vex objects.
// vex objects are identified by their checksum
const vexstore = new class visibleSchedulesClass {
	constructor() {
		this.prevcurnext = [undefined, undefined, undefined];
	}

	// getters return empty object if undefined
	// this is necessary because code using this doesn't check for undefined.
	get prev() { let e = this.prevcurnext[0]; return (e != undefined)?e:{} }
	get cur() { let e = this.prevcurnext[1]; return (e != undefined)?e:{} }
	get next() { let e = this.prevcurnext[2]; return (e != undefined)?e:{} }

	// Check if datastore.knownvexfiles contains new relevant info.
	// Download any vexfiles that were not parsed before.
	update() {
		const tnow = new Date().getTime();
		const pcn = [undefined, undefined, undefined];
		for (let i in datastore.knownvexfiles) {
			let e = datastore.knownvexfiles[i]
			// historic
			if (vex2time(e.stopvex) < tnow) {
				pcn[0] = e
				continue
			}
			// future
			if (vex2time(e.startvex) > tnow) {
				pcn[2] = e
				break
			}
			// current
			pcn[1] = e
		}

		// use a parsed object if available, otherwise download new
		let j
		let promises = []
		for (let i in pcn) {
			let e = pcn[i]
			// no longer needed
			if (e == undefined) {
				this.prevcurnext[i] = undefined
				continue
			}
			// find needed object in previously parsed data
			for (j = i; j < this.prevcurnext.length; j++) {
				if (this.prevcurnext[j] == undefined) continue
				if (this.prevcurnext[j].checksum != e.checksum) continue
				this.prevcurnext[i] = this.prevcurnext[j]
				break
			}

			// no parsed object found, get the new schedule from the server
			if (j == this.prevcurnext.length) {
				let promise = getSchedule(e.filename).then(res => this.prevcurnext[i] = res)
				promises.push(promise)
			}
		}
		return Promise.all(promises)
	}
}


var datastore = new function() {
	this.data = {};
	this.metadata = {};
	this.schedule = {};
	this.knownvexfiles = [];
	this.id_replay = "none";
	this.messages = {};
	this.clients = {};
	this.dependencies = {};
	this.visibility = {
		observatories: {}
	};

	//this.metadatadeferred = $.Deferred();
	//this.initdeferred = $.Deferred();

	Cookies.remove("snap_recvTime")

	// what should be updated:
	this.config = {
		showcountdown: false,

		messageFacility: window.localStorage.getItem("messageFacility"),  // facility to post messages as:

		observatories: [],	// empty list means all
		fields: [],			// empty list means all

		// which form field to update when interval changes, must be a valid jquery selector
		intervalFormField: undefined
	}

	var self = this; // self is required because this may refer to the function calling context

	// private members:
	//var callbacks = {data:[],metadata:[],schedule:[],all:[]};
	var callbacks = [];
	var updateinterval;
	var timer;
	var lastScheduleUpdate = 0;
	var nextUpdate = 0;

	function initVisibilities() {
		// fetch visibility from cookie
		self.visibility.observatories = window.localStorage.getObject("observatoryvisibility") || {};
		// if in single observatory mode, filter:
		if(getURLParameter("observatory") != undefined) {
			$.each(self.visibility.observatories, function(name, value) {
				self.visibility.observatories[name] = false;
			});
			// this way we always set the url provided obs to true, even if it isn't in the cookie yet because
			// this is somehow the first page the user visits
			self.visibility.observatories[getURLParameter("observatory")] = true;
		}

		// determine schedule visibility
		self.visibility.schedules = window.localStorage.getObject("schedulevisibility") || {};
	}

	function initCountdownButton() {
		if(self.config.showcountdown) {
			// <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
			//	 <i class="material-icons">add</i>
			// </button>
			$(document).ready(function() {
				// remember when the first update will happen
				nextUpdate = new Date(new Date().getTime()+1000*updateinterval);

				// create the button
				var countdownarea = $("<div>").addClass("countdownarea");
				var countdownbutton = $("<button>");

				// main button
				countdownbutton.addClass("countdownbutton mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent mdl-shadow--4dp");
				var remaining = 0.5+(nextUpdate - new Date())/1000;//updateinterval - (new Date()-lastUpdate)/1000;
				countdownbutton.html(sprintTime(remaining));

				// handle mouse events:
				countdownbutton.on("mouseenter", function(e) {
					$(".countdownarea").addClass("expanded");
				});
				countdownarea.on("mouseleave", function(e) {
					$(".countdownarea").removeClass("expanded");
				});

				// handle touch events
				// countdownbutton.on({ 'touchstart' : function(e){
				//	 console.log("touched");
				// }});
				countdownbutton.on('touchend', function(e) {
					//console.log("touched ended");
					$(".countdownarea").toggleClass("expanded");
					// the following may be confusing:
					// on touch devices the touchend may trigger a mouseenter, mouseleave or click depending on previous touch events in a way that emulates mouse events
					// when the user clicks refresh or changes the update frequency a mouseleave is generated for the countdownbutton
					// the following prevents a mousenter which would immediately reopen the thing if it was already open (and the above tobble closed it)
					if(!$(".countdownarea").hasClass("expanded")) {
						e.preventDefault();
					}

				});



				// refresh button
				var refreshbutton = $("<button>").html("<i class='material-icons'>refresh</i>");
				refreshbutton.addClass("refreshbutton mdl-button mdl-js-button mdl-button--fab mdl-shadow--4dp");
				refreshbutton.click(function() { self.update(); });


				// buttons for update interval
				var data = [1,3,10,60,180,600];
				var selection = d3.select(countdownarea.get(0)).selectAll(".timebutton").data(data);
				selection.enter().append("button")
					.attr("class","timebutton mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-shadow--4dp")
					.classed("active",function(d) {
						return d==updateinterval;
					})
					.html(function(d){return sprintTime(d)})
					.on('click',function(d) {
						self.setUpdateInterval(d);
						selection.classed("active",function(d1) {
							return d1==updateinterval;
						})
					})
					.each(function(d) {
						componentHandler.upgradeElement(this);
					});

				countdownarea.append(refreshbutton);
				countdownarea.append(countdownbutton);




				componentHandler.upgradeElement(countdownbutton.get(0));
				componentHandler.upgradeElement(refreshbutton.get(0));

				$("body").append(countdownarea);

				// schedule button text updates:
				setInterval(function() {
					var remaining = 0.5+(nextUpdate - new Date())/1000;//updateinterval - (new Date()-lastUpdate)/1000;
					$("button.countdownbutton").html(sprintTime(remaining));
				},1000);
			});
		}
	}

	function makeDependencyMap() {
		for (var observatoryname in datastore.metadata) {
			// prepare storage for dependency graph
			datastore.dependencies[observatoryname] = {in: {}, out: {}, arglist: {}, order: []}; // both in and out are dictionaries
			// determine all valid object names
			var fieldNames = Object.keys(datastore.metadata[observatoryname]).concat(Object.keys(datastore.metadata[observatoryname]).map(x=>x+"_valid"));
			fieldNames.push("x");
			fieldNames.push("children");
			fieldNames.push("schedule");
			// prepare empty sets for bidi map:
			// for the flattening algorithm we need both a forward and a reverse dependency map:
			for (let i in fieldNames) {
				let fieldname = fieldNames[i];
				datastore.dependencies[observatoryname].in[fieldname] = new Set();
				datastore.dependencies[observatoryname].out[fieldname] = new Set();
				datastore.dependencies[observatoryname].arglist[fieldname] = [];
			}

			for (let fieldname in datastore.metadata[observatoryname]) {
				for (let func in {"valueFunction":undefined,"validFunction":undefined}) {
					if (func in datastore.metadata[observatoryname][fieldname]) {
						let functext = datastore.metadata[observatoryname][fieldname][func];
						try {
							datastore.metadata[observatoryname][fieldname][func] = eval(functext);
						} catch(e) {
							console.warn(`The ${func} function for ${fieldname} in ${observatoryname} ran into an error.`)
							console.warn("raw function text: "+functext)
							console.warn(e)

							continue;
						}
						if (typeof(datastore.metadata[observatoryname][fieldname][func]) != "function") {
							console.warn(`${func} parsed correctly but the result is not a function!`)
							console.warn("raw function text: "+functext)
							delete datastore.metadata[observatoryname][fieldname][func];
							continue;
						}
						// if we reach this point a parsable function was present

						// determine dependencies
						let arglist = datastore.metadata[observatoryname][fieldname][func].toString().split("=>")[0];
						let argtokens = arglist.split(/([^a-zA-Z0-9_])/).filter(arg => fieldNames.indexOf(arg) >=0);

						// save argument list to datastore for later reference.
						let isvalid = func=="validFunction";
						let fullvarname = isvalid ? (fieldname+"_valid") : fieldname;
						datastore.dependencies[observatoryname].arglist[fullvarname] = argtokens;

						// replace "x", "schedule", and "children" in dependencies
						argtokens = $.map(argtokens, function(arg) {
							if (arg == "x") return fieldname;
							if (arg == "schedule") return undefined; // i.e. skip
							if (arg == "children") {
								let children = $.map(fieldNames, childname => {
									if (childname.endsWith("_valid")) return undefined; // it does depend on that but that's added later
									// childname and fieldname neither have _valid now
									if (!childname.startsWith(fieldname)) return undefined; // not a decendant
									if (childname==fieldname) return undefined; // should not be the same
									let basedepth  = (fieldname.match(/_/g)||[]).length;
									let childdepth = (childname.match(/_/g)||[]).length;
									if (childdepth == basedepth+1) { // immediate child
										if (isvalid) {
											// for validfunctions we assume that depending in "children" also includes a dependency on the validity of those children
											//return [childname, childname+"_valid"];
											return childname+"_valid";
										} else {
											// for value functions we assume that dependency on the children only means dependency on the values
											return childname;
										}
									}
									return undefined; // skip anything else
								});
								return children;
							}
							// else, return unchanged
							return arg;

						});

						for (let i in argtokens) {
							let other = argtokens[i];
							datastore.dependencies[observatoryname].out[fullvarname].add(other);
							datastore.dependencies[observatoryname].in[other].add(fullvarname);
						}
					}
				}
			}

			// make dependency-free ordering:
			// this list is calculated once and used to make sure updates are always
			// executed in an order that respects the dependencies.
			// pseudo-algorithm is as follows:
			// take any element that has no dependencies
			// add it to the list
			// remove it from the dependency graph
			// repeat.
			// (if the graph is acyclic it is guaranteed that this always works)
			datastore.dependencies[observatoryname].order = [];

			// clone the sets because we are going to mutilate them:
			let copyin = {};
			for (v in datastore.dependencies[observatoryname].in) {
				copyin[v] = new Set(datastore.dependencies[observatoryname].in[v]);
			}
			let copyout = {};
			for (v in datastore.dependencies[observatoryname].out) {
				copyout[v] = new Set(datastore.dependencies[observatoryname].out[v]);
			}

			while (Object.keys(copyout).length > 0) {
				for (let v in copyout) {
					if (copyout[v].size == 0) {//  <------------------------------------------|
						// add it to the list:												  |
						datastore.dependencies[observatoryname].order.push(v);//			  |
						// remove it from the graph:										  |
						copyin[v].forEach(function(p) {	copyout[p].delete(v);});//			  |
						// copyout[v].forEach(function(p){ copyin[p].delete(v);}); not needed because copyout is guaranteed to be empty
						delete copyout[v];
						delete copyin[v];
					}
				}
			}

			// cleanup
			delete datastore.dependencies[observatoryname].out["x"];
			delete datastore.dependencies[observatoryname].out["children"];
			delete datastore.dependencies[observatoryname].out["schedule"];

			delete datastore.dependencies[observatoryname].in["x"];
			delete datastore.dependencies[observatoryname].in["children"];
			delete datastore.dependencies[observatoryname].in["schedule"];


			let i = datastore.dependencies[observatoryname].order.indexOf("x");
			if (i>=0) {
				delete datastore.dependencies[observatoryname].order.splice(i, 1);
			}
			i = datastore.dependencies[observatoryname].order.indexOf("children");
			if (i>=0) {
				delete datastore.dependencies[observatoryname].order.splice(i, 1);
			}
			i = datastore.dependencies[observatoryname].order.indexOf("schedule");
			if (i>=0) {
				delete datastore.dependencies[observatoryname].order.splice(i, 1);
			}
		}
	}

	const g_has = Object.prototype.hasOwnProperty
	//-- Station names defined in the schedule
	const g_stationname_vex = {
		"ALMA" : "ALMA",
		"AMT"  : "AMT",
		"APEX" : "APEX",
		"GLT"  : "THULE",
		"JCMT" : "JCMT",
		"KP"   : "KITTPEAK",
		"LMT"  : "LMT",
		"NOEMA": "NOEMA",
		"PICO" : "PICOVEL",
		"SMA"  : "SMAP",
		"SMTO" : "SMTO",
		"SPT"  : "SPT",
		}


	function recomputeFunctions() {
		window.allvalid = function(fields) {
			let res = true;
			for (let i in fields) {
				let fieldname = fields[i];
				if (!datastore.data[observatoryname][fieldname].valid) {
					res = false;
					datastore.data[observatoryname][basename].reason.push(
						...datastore.data[observatoryname][fieldname].reason
					);
				}
			}
			return res;
		};

		window.allvalidormissing = function(fields) {
			let res = true;
			for (let i in fields) {
				let fieldname = fields[i];
				//let basename = fieldname.match(/(.*)_valid/) || fieldname;
				if (datastore.data[observatoryname][fieldname].value == undefined) continue;
				if (!datastore.data[observatoryname][fieldname].valid) {
					res = false;
					datastore.data[observatoryname][basename].reason.push(
						...datastore.data[observatoryname][fieldname].reason
					);
				}
			}
			return res;
		};

		window.stationisactiveinscan = function(sta = undefined, scanid = undefined) {
			if (sta === undefined) sta = observatoryname

			let currentitem = getCurrentScheduleItem();
			if (scanid == undefined && currentitem != undefined) scanid = currentitem.ScanId;
			if (scanid == undefined) return false;

			if (datastore.schedule.$SCHED == undefined) return false;

			let scan = datastore.schedule.$SCHED[scanid];
				
			// first find the abbreviation: // TODO do this once and build a map for fast lookup
			if (g_has.call(g_stationname_vex, sta)) sta = g_stationname_vex[sta]
			let abbrv = undefined;
			for (let a in datastore.schedule.$STATION) {
				if (datastore.schedule.$STATION[a].site_name == sta) {
					abbrv = a;
					break;
				}
			}
			// sanity check
			if (abbrv == undefined) {
				// if a station does not occur in the schedule then it cannot be active
				return false;
			}
			// now check if the current scan has this station
			let now = new Date().getTime();
			
			// this is the current scan: check if the station participates
			for (let station of scan.station) {
				if (station.$STATIONkey == abbrv) {
					return true;
					//return scan.start + 1000*station.datastart < now && scan.start + 1000*station.datastop > now;
				}
			}
			// none of the stations matched
			return false;
		};

		window.stationisactiveinexperiment = function(sta = undefined) {
			if (sta == undefined) sta = observatoryname;
			
			if (datastore.schedule.$SCHED == undefined) return false;
			// first find the abbreviation: // TODO do this once and build a map for fast lookup
			if (g_has.call(g_stationname_vex, sta)) sta = g_stationname_vex[sta]
			let abbrv = undefined;
			for (let a in datastore.schedule.$STATION) {
				if (datastore.schedule.$STATION[a].site_name == sta) {
					abbrv = a;
					break;
				}
			}
			// sanity check
			if (abbrv == undefined) {
				// if a station does not occur in the schedule then it cannot be active
				return false;
			}

			// now see if any scan in this experiment references this station
			/*for (let scanid in datastore.schedule.$SCHED) {
				for (let i in datastore.schedule.$SCHED[scanid].station) {
					if (datastore.schedule.$SCHED[scanid].station[i].$STATIONkey == abbrv) return true;
				}
				}*/

			// new implementation: also check that its not had it's last scan or not yet started it's first scan:
			var firststart = 9999999999999;
			var lastend = 0;
			for (let scan of Object.values(datastore.schedule.$SCHED)) {
				if (scan.start < firststart && scan.station.map(x=>x.$STATIONkey).includes(abbrv))
					firststart = scan.start;
				if (scan.stop > lastend && scan.station.map(x=>x.$STATIONkey).includes(abbrv))
					lastend = scan.stop;
			}
			// if none of the stations in none of the scans:
			var now = new Date().getTime();
			return firststart < now && lastend > now;
		};

		//console.log("recomputing...")
		for (var observatoryname in datastore.metadata) {
			for (var i in datastore.dependencies[observatoryname].order) {
				var varname = datastore.dependencies[observatoryname].order[i];
				// TODO: skip the functions that aren't really necessary
				// TODO: skip functions that are not visible and have no visible derivates
				// TODO: skip functions of which no dependency actually changed in the update
				if (varname == "schedule") continue;
				var isvalid = varname.endsWith("_valid");
				var basename = isvalid?varname.substr(0,varname.length-6):varname;
				// now test if anything can be done at all
				var func = datastore.metadata[observatoryname][basename][isvalid?"validFunction":"valueFunction"];
				if (func == undefined) {
					// skip evaluation of non-existent validFunction,
					// but also insert 'true' (default) in the place of the result
					if (isvalid) {
						datastore.data[observatoryname][basename].valid = true;
					}
					continue;
				}
				var args = $.map(datastore.dependencies[observatoryname].arglist[varname], function(x) {
					// all return statements are wrapped in size-1 arrays to prevent jquery from skipping/flattening items
					// (an undefined value would be skipped and an array would be flattened)
					if (x=="x") return [datastore.data[observatoryname][basename].value];
					if (x=="schedule") return [{
						'isactiveinscan': window.stationisactiveinscan,
						'isactiveinexperiment': window.stationisactiveinexperiment,
						'currentExperiment': getCurrentExperiment(),
						'currentScan': getCurrentScheduleItem()
					}]; 
					if (x=="children") {
						var children = [];
						for (var othername in datastore.metadata[observatoryname]) {
							if (othername.startsWith(basename)) {
								if ((basename.match(/_/g)||[]).length +1 == (othername.match(/_/g)||[]).length) { // check that level is exactly 1 deeper
									children.push(othername);
								}
							}
						}
						return [children];
					}
					if (x.endsWith("_valid")) {
						return [datastore.data[observatoryname][x.substr(0, x.length-6)].valid];
					} else {
						return [datastore.data[observatoryname][x].value];
					}
				});

				var thisvar = {};
				window.max = Math.max;
				window.min = Math.min;
				try {
					if (isvalid) {
						// first, reset the invalid-reasons list
						datastore.data[observatoryname][basename].reason = [];
						// then execute the function
						let validity = func.apply(thisvar, args);
						// save the result
						datastore.data[observatoryname][basename].valid = validity;
						// if it failed
						if (!validity) {
							// append the aggregate reason
							let msg = "";
							if (datastore.data[observatoryname][basename].value != undefined) {
								msg = `value ${basename}=${datastore.data[observatoryname][basename].value}`
							} else {
								msg = `${basename}`;
							}
							msg = msg.concat(" did not pass check:");
							msg = msg.concat(` ${func.toString()}`);
							if (datastore.data[observatoryname][basename].reason.length==0)
								datastore.data[observatoryname][basename].reason.push(msg);
							// else, skip because validfunction already set the reason
						}

					} else {
						datastore.data[observatoryname][basename].value = func.apply(thisvar, args);
						datastore.data[observatoryname][basename].time	= new Date().getTime()/1000;
					}
				} catch (e) {
					console.warn(`error in function execution for ${varname} in ${observatoryname}`)
					console.warn(e)
					console.log(args)
					console.log(func.toString())
				}
			}
		}
	}


	/* init() return a promise that is resolved when the metadata is fetched and processed
	   and when the first update is complete. This assumes it makes no sense to render anything
	   as long as there is no data. However, it is guaranteed that this promise will be resolved before
	   handlers registered via onUpdate are called for the first time, so preparatory
	*/
	var metadatadeferred = $.Deferred();
	var initdeferred = $.Deferred();

	this.init = function() {
		// initially get the interval from cookie:
		updateinterval = window.localStorage.getItem("refreshrate") || "3";
		// show in form if it exits:
		$(document).ready(function() {
			$(self.config.intervalFormField).val(updateinterval);
		});


		initVisibilities();
		initCountdownButton();

		// fetch metadata once
		fetchMetadata().done(()=>metadatadeferred.resolve());

		// start updating
		self.update();

		return initdeferred.promise();
	}




	// methods to change the update interval
	this.setUpdateInterval = function(interval) {
		var prevvalue = updateinterval;
		updateinterval = parseInt(interval);
		if (updateinterval < prevvalue && metadatadeferred.state()!='pending') {self.update()};
		window.localStorage.setItem("refreshrate", updateinterval);
		$(self.config.intervalFormField).val(updateinterval);
	}
	this.getUpdateInterval = function() {
		return updateinterval;
	}
	this.setFacility = function(facility) {
		// if(facility=="undefined") facility=undefined;
		self.config.messageFacility = facility;
		window.localStorage.setItem("messageFacility", self.config.messageFacility)

		// when the current page is the messaging page this will trigger a rerender of the warning
		if(typeof(updateTagWarnings)=="function") {
			updateTagWarnings();
		}
	}


	this.onUpdate = function(callback) {
		callbacks.push(callback);
	}


	this.update = function() {
		// cancel in case this was run from some user interaction command (e.g. changing the update freq.)
		clearInterval(timer);
		$("#updatespinner").show();


		fetchData().done(function() {
			$.when(vexstore.update(), self.metadataDeferred).done(function() {
				self.schedule = vexstore.cur
				// execute value/valid functions
				recomputeFunctions();
				// tell the page that data is available for rendering
				initdeferred.resolve();
				// call the callbacks
				$.each(callbacks,function(i,c){c()});
				// clear feeback in gui
				$("#updatespinner").hide();
			}).fail(function() {
				showMessage("Server communication error");
			});
		});


		timer = setTimeout(self.update, 1000*updateinterval);
		nextUpdate = new Date(new Date().getTime()+1000*updateinterval);
	}



	/* perform an xhr json request for data. After data is received it is saved and value/valid resolution is performed.
	   For the latter it waits for metadata to have also arrived (even though the requests are fired asynchonously).
	   This method immediately returns a deferred object that resolves after valueresolution is complete. */
	function fetchData() {

		var deferred = $.Deferred(); // extra deferred because we don't want to resolve data after the request
		// but after the data has been processed which also depends on metadata

		$.ajax('/data/snapshot', {dataType: 'json', statusCode: {
			401: () => {
				window.localStorage.setItem('returnurl', window.location.href)
				window.location.replace('/login.html')
			}
		}})
		.done(function(response, textStatus, request) {
			// update stream time
			let id_replay = request.getResponseHeader('replay-id')
			if (! id_replay) self.id_replay = void 0

			let tstream
			// replay mode, use data timestamps
			if (id_replay != "") {
				tstream = datatime_max(response)*1000
			}
			// normal mode, use server time
			else {
				let tserver = request.getResponseHeader('date')
				tstream = new Date(tserver).getTime()
			}

			// set time
			// new stream, unconstrained set
			let now = new Date()

			// allow large corrections on new replay
			if (id_replay && (id_replay != self.id_replay)) {
				let dt = tstream - now.getTime()
				// give the stream clock time to settle: the first packet may contain non-stream data points (a complete array-wide snapshot)
				if (tstream != 0) {
					if (Math.abs(dt) < 10*1000) self.id_replay = id_replay
					setStreamtime(tstream)
				}
			}
			// continued stream, correct for transmission delays only
			else {
				if (tstream > now.getTime()) {
					setStreamtime(tstream)
				}
			}

			// save the data
			metadatadeferred.done(function() {
				$.each(response, function(observatoryname, observatorydata) {
					if (! (observatoryname in datastore.metadata)) return; // skip e.g. vexfiles
					var newparameters = {};
					for (var varname in observatorydata.data) {
						newparameters[varname] = new parameter(observatorydata.data[varname][0], observatorydata.data[varname][1]);
					}
					// the explicit assignment is necessary here in case the data is undefined (extend does not know how to overwrite undefined objects)
					self.data[observatoryname]	   = $.extend(self.data[observatoryname], newparameters);
					//self.data[observatoryname]	 = $.extend(self.data[observatoryname], $.map());
					self.clients[observatoryname]  = $.extend(self.clients[observatoryname], observatorydata.clients);
					self.messages[observatoryname] = $.extend(self.messages[observatoryname], observatorydata.messages);
					//console.log("data processed");
				});

				// save the list of known vex filenames
				if (!Object.prototype.hasOwnProperty.call(response, 'vexfiles')) {
					deferred.resolve();
					return
				}

				self.knownvexfiles = self.knownvexfiles.concat(response.vexfiles);
				// sort
				self.knownvexfiles.sort((a, b) => {
					if (a.startvex < b.startvex) return -1;
					if (a.startvex > b.startvex) return 1;
					return 0;
				});
				// filter overlapping vex files, keep newest
				self.knownvexfiles = self.knownvexfiles.reduce((a, e) => {
					// first element
					if (a.length == 0) return [e];
					const prev = a[a.length-1];
					// overlap, keep latest assuming the old one was replaced
					if (e.startvex < prev.stopvex) {
						const dropped = a.pop();
						console.log(`vexinfo.getSeq: drop '${dropped.filename}' due to overlap with '${e.filename}'`);
					}
					return a.concat(e);
				}, []);

				deferred.resolve();
			});
		})
		.fail(function(){
			showMessage("Server communication error");
		});

		return deferred.promise();
	};


	function fetchMetadata() {
		var deferred = $.Deferred();
		$.getJSON("/static/masterlist.json", function(response, status) {

			// cookie visibility is stored in the cookie and handled separately from the current page visibility
			// since the current page may be in 'single observatory mode' (by setting ?observatory= url param)
			var cookievisibility = window.localStorage.getObject("observatoryvisibility") || {};

			defaultmetadata = response["default"];
			$.each(response, function(observatoryname, observatorydata) {
				if (observatoryname == "default") {
					return; // skip it, it's not a real observatory
				}

				if(cookievisibility[observatoryname]==undefined) {
					// new observatories default to visible
					cookievisibility[observatoryname] = true;
					//-- default visibility OFF for future stations
					if (['AMT'].includes(observatoryname)) {
						cookievisibility[observatoryname] = false;
					}
					// if the current page is not single-observatory mode, then add it to this page as well
					if(getURLParameter("observatory")==undefined) {
						self.visibility.observatories[observatoryname] = true;
					}
				}
				self.metadata[observatoryname] = $.extend(true, {}, defaultmetadata, observatorydata);
				self.data[observatoryname] = {};
				$.each(self.metadata[observatoryname], function(varname) {
					self.data[observatoryname][varname] = new parameter();
				});


				/*$.each(observatorydata, function(fieldname, fielddata) {
				  Object.assign(self.metadata[observatoryname][fieldname], fielddata);
				  });*/
			});
			window.localStorage.setObject("observatoryvisibility", cookievisibility);

			// make dependency map
			makeDependencyMap();
			// I created a separate deferred to make 100% sure that this is done before anything else is executed
			//console.log("metadata done")
			deferred.resolve();
		});

		// again return the deferred object
		return deferred;
	};
}

// get the latest sendTime known by the server
// TODO: return undefined instead of zero on no data
function datatime_max(response) {
			return lastRecvTime = Object.entries(response).reduce((a, e) => {
				const [k, v] = e
				if (!Object.prototype.hasOwnProperty.call(v, "data")) return a
				const max = Object.values(v.data).reduce((a, e) => {
					value = e[0]
					if (value > a) return value
					return a
				}, 0)
				if (max > a) return max
				return a
			}, 0)
}

// vim: noet
