/* These are some common functions used to resolve "derivative" parameter inside a scope (js closure) such that they cannot be confused with global stuff
 * Copyright Radiolab 2017
 */


 // this list contains the reserved javascript names but also the "" to
 // ensure that noone will every attempt to use it as a variablename
 // it also includes common expressions that should be evaded as variable names although js does not forbid it (but may behave weirdly after overwriting)
 var reservedJSWords = [
 "","abstract","arguments","await","boolean","break","byte","case",
 "catch","char","class","const","continue","debugger","default",
 "delete","do","double","else","enum","eval","export","extends",
 "false","final","finally","float","for","function","goto","if",
 "implements","import","in","instanceof","int","interface","let",
 "long","native","new","null","package","private","protected","public",
 "return","short","static","super","switch","synchronized","this",
 "throw","throws","transient","true","try","typeof","var","void",
 "volatile","while","with","yield","abstract","boolean","byte","char",
 "double","final","float","goto","int","long","native","short",
 "synchronized","throws","transient","volatile","Array","Date","eval",
 "function","hasOwnProperty","Infinity","isFinite","isNaN",
 "isPrototypeOf","length","Math","NaN","name","Number","Object",
 "prototype","String","toString","undefined","valueOf","alert","all",
 "anchor","anchors","area","assign","blur","button","checkbox",
 "clearInterval","clearTimeout","clientInformation","close","closed",
 "confirm","constructor","crypto","decodeURI","decodeURIComponent",
 "defaultStatus","document","element","elements","embed","embeds",
 "encodeURI","encodeURIComponent","escape","event","fileUpload",
 "focus","form","forms","frame","innerHeight","innerWidth","layer",
 "layers","link","location","mimeTypes","navigate","navigator",
 "frames","frameRate","hidden","history","image","images",
 "offscreenBuffering","open","opener","option","outerHeight",
 "outerWidth","packages","pageXOffset","pageYOffset","parent",
 "parseFloat","parseInt","password","pkcs11","plugin","prompt",
 "propertyIsEnum","radio","reset","screenX","screenY","scroll",
 "secure","select","self","setInterval","setTimeout","status","submit",
 "taint","text","textarea","top","unescape","untaint","window"
 ];

// this is configurable in the server as well but changing will requires massive renaming in the database and is for that reason discouraged ;)
var FieldSeparator = "_";




/** small helper that replaces : by - such that vriablenames can be used as row class attribute
: has a special meaning in css selectors so it cannot be used in class attributes. */
/*function cssFriendlyVariablename(variablename) {
	return variablename.split(FieldSeparator).join('-');
}*/

/** small helper that replaces : by _ such that vriablenames can be used inside jvascript snippets */
/*function jsFriendlyVariablename(variablename) {
	var tmp = $.trim(variablename);
	tmp = tmp.split(FieldSeparator).join('_');
	if(reservedJSWords.indexOf(tmp)>=0) {
		tmp= tmp + "_";
	}
	return tmp;
}*/





/* makeEvalContext creates the actual closure scope in which each valueFunction/validFunction is executed. */
function makeEvalContext(variablenames) {
	var ctx = {};

	// the following declaration provide mediocre protection against accidentally accessing stuff that valueFunc and validFunc shouldn't read/write
	// this is by no means 'secure'. It's just a protection against typo's and unexpected behaviour.
	var window,
	document,
	ActiveXObject,
	XMLHttpRequest,
	alert,
	setTimeout,
	setInterval,
	clearTimeout,
	clearInterval,
	Function;

	var x;	 		// will hold the value to compare when calling validFunction
	var valid;		// this declaretion just creates a slot for a function that will be executed in the right scope/closure. It can be set later
    var invalid;
    var all;		// accessor for other observatories
	var children;	// place to store list of childnames
	var schedule;	// place to store the scheduledata
    var observatoryname; //

	// old fashioned for-loop because forEach(function(){}) creates closures hiding the newly created variables from te desired scope
	for (var i = 0; i < variablenames.length; i++) {
	    // console.log("declaring variable "+variablenames[i]+" as "+jsFriendlyVariablename(variablenames[i]));
	    if (reservedJSWords.indexOf(variablenames[i])<0) {
		eval("var "+variablenames[i]+";");
	    }
	    eval("var "+variablenames[i]+"_valid;");
            eval("var "+variablenames[i]+"_invalid;");
	}
	// there is now a slot for each (derivative) parameter.

	// the getter allows us to retrieve stuff from the closure scope
	function get(name) {
		// console.log("get called:")
	    // console.trace()
	    if (reservedJSWords.indexOf(name)>=0)
	    {   // don't attempt to get the value of parameters that coincide with js keyword. Most notaby: if
		return undefined;
	    }
	    
		var val = eval(name);
		if(typeof(val)=="function") {
			var varname = name.endsWith("_valid")?name.slice(0,-6):name;
			// set x
			if(name.endsWith("_valid")) {
				x = get(varname);
				// if there is no data: then default validity true
				// TODO: implement a better default. Could be "allvalid(children)"
				// which would, in case of no children also return true
				// but it is a better default if there are children...
				// if (x==undefined) return true;
			}// else there is no sense in setting x
			// set children
			children = $.map(variablenames, function(childname) {
				if(childname.startsWith(varname) && childname != varname && childname.slice(varname.length+1).indexOf(FieldSeparator)<0) {
                    return childname;
				} else {
					return undefined; // skip this variable
				}
			});
			return val();
		} else {
			return val;
		}
		// return ctx[name];	// actually equivalent
	}



	var allvalid = (function(includes, excludes=[]) {
		var inval = [];
		var varname = includes[0].split(FieldSeparator).slice(0,-1).join(FieldSeparator);
		for(var i in includes) {
			var field = includes[i];
			if(excludes.indexOf(field) >=0) continue;
			var valid = get(field+"_valid");
			try {
				var invalid = get(field+"_invalid");
			} catch(e) {
				var invalid = undefined;
			}
			if(invalid!=undefined && valid!=undefined && !valid) inval.push(...invalid);
		}
		if(inval.length > 0) {
			set(varname+"_invalid", inval);
		}
		return inval.length == 0;
	}).bind(ctx);// allvalid requires references to the other

	var allvalidormissing = (function(includes, excludes=[]) {
		var inval = [];
		var varname = includes[0].split(FieldSeparator).slice(0,-1).join(FieldSeparator);
		for(var i in includes) {
			var field = includes[i];
			if(excludes.indexOf(field) >=0) continue;
			var value = get(field);
			if(value==undefined) continue;
			var valid = get(field+"_valid");
			if(valid!=undefined && !valid) inval.push(field + '=' + value)
		}
		if(inval.length > 0) {
			set(varname+"_invalid", inval);
		}
		return inval.length == 0;
	}).bind(ctx);// allvalid requires references to the other

	var max = (function(re) {
		var res = NaN;
		for(var field in this) {
			// we cannot reliably turn the regex into jsfriendly variables
			// fortunately : has no (very) special meaning in regexes (as opposed to js)
			// and we can turn the fieldname back into : separated form
			// this fails for fieldnames that contain '_' characters.
			// it also fails for field names that exactly match a reserved js keyword
			// since jsFriendlyVariablename adds an extra _ to those that the end
			//var varname = field.replace(/_/g,":");
			var match = field.match(re);
			// check for an exact match
			if(match != null && field == match) {
				if(!(res > get(field))) {
					// Note that when res is initially NaN all comparators return false
					res = get(field);
				}
			}
		}
		if(res != res) return undefined;
		return res;
	}).bind(ctx);//  requires references to the other varibles

	var min = (function(re) {
		var res = NaN;
		for(var field in this) {
			//var varname = field.replace(/_/g,":");
			var match = field.match(re);
			if(match != null && field == match) {
				if(!(res < get(field))) {
					res = get(field);
				}
			}
		}
		if(res != res) return undefined;
		return res;
	}).bind(ctx);


	var avg = (function(re) {
		var total = 0;
		var count = 0;
		for(var field in this) {
			//var varname = field.replace(/_/g,":");
			var match = field.match(re);
			// check for an exact match
			if(match != null && field == match) {
				total += get(field);
				count += 1;
			}
		}
		return count>0?total/count:undefined;
	}).bind(ctx);


    // the observatory is currently active in a scan
    var isactiveinscan = (function() {
        // console.log(this.observatoryname);
        if (this.schedule.currentScan == undefined) return false;
        var namemap = {
            'ALMA':'Aa',
            'APEX':'Ax',
            'PICO':'Pv',
            'SPT':'Sz',
            'LMT':'Lm',
            'SMTO':'Mg',
            'JCMT':'Mm',
            'SMA':'Sw',
            'GLT':'Gl',
	    'KP':'Kp'
        };
        for (var i in this.schedule.currentScan.Stations) {
            if (this.schedule.currentScan.Stations[i].StationName==namemap[this.observatoryname]) return true;
        }
        return false;
    }).bind(ctx);


    // the observatory is currently active in the experiment
    var isactiveinexperiment = (function() {
        if (getCurrentExperiment() == undefined) return false;

        var namemap = {
            'ALMA':'Aa',
            'APEX':'Ax',
            'PICO':'Pv',
            'SPT':'Sz',
            'LMT':'Lm',
            'SMTO':'Mg',
            'JCMT':'Mm',
            'SMA':'Sw',
            'GLT':'Gl'
        };
        var data = datastore.schedules[getCurrentExperiment().File].ScheduleData;
        var tnow = new Date();
        var wasactive = false;
        var willbeactive = false;
        for (var i in data.Scans) {

            for (var j in data.Scans[i].Stations) {
                //is active
                if (data.Scans[i].Stations[j].StationName == namemap[this.observatoryname]) {
                    var tstart = new Date(data.Scans[i].StartTime);
                    var tend = new Date(data.Scans[i].StopTime);
                    //current scan
                    if (tnow > tstart  &&  tnow < tend) return true;
                    //past scans
                    if (tnow > tstart) wasactive = true;
                    //future scans
                    if (tnow < tend) willbeactive = true;
                    // in between of active scans
                    if (wasactive && willbeactive) return true;
                }
            }
        }
        return false;
    }).bind(ctx);

	// this setter allows us to later add/edit stuff in the closure scope
	function set(name, val) {
		// console.log("attempting to set \""+name+"\" for \""+get("observatory_name")+"\" to \""+val+"\"");
		try {
			var current = eval(name);
			if(typeof(current)=="function") {
				// console.warn("WARNING: data attempted to overwrite functional parameter "+name+" with a constant value. Overwrite aborted!!");
				return;
			}
		}
		catch(e) {
			// not so bad, it just wasn't defined
		}

		if(variablenames.indexOf(name)<0 && !name.endsWith("_valid") && !name.endsWith("_invalid") && name !="x" && name !="children" && name != "schedule" && name != "observatoryname") {
			console.warn("WARNING: saving "+name+" in global scope!");
		}
		try {
			// usually the value is a string taken from the masterlist
			// but it may be convenient to be able to pass objects or arrays as well
			// this is, for instance, used to set the schedule and the children
			// parameters before execution of functions.
			if(typeof(val)=="object") val = JSON.stringify(val);
			// we assign the result to a temporary variable so that we can check its type
			var newparametervalue = eval(name+" = "+val);
			if(typeof(newparametervalue)=="function") {
				// console.log("binding to ");
				// console.log(ctx);
				eval(name+"="+name+".bind(ctx);");
			}
		}catch(e) {
			if (val != "") {
				console.warn("invalid js expression for \""+name+"\" in "+get("observatory_name")+": \""+val+"\" resulted in the following error: ");
				console.warn(e);
			}
		}
		// the following creates easy access to the variables
		ctx[name]=eval(name);
	}

	function getValidFunc(name) {
		return eval(name+"_valid");
	}

	// the ctx object which is returned is just a wrapper for the get and set methods
	ctx.get = get;
	ctx.set = set;
	ctx.getValidFunc = getValidFunc;


	// add handy function to add metadata and data (upon arrival)
	/* the initEvalContext function put everything from a metadata requests response in the context */
	ctx.initEvalContext = function(facilitymetadata) {
		// console.warn(facilitymetadata);
		$.each(facilitymetadata,function(variablename, vardata) {
			// console.log(vardata);
			if("valueFunction" in vardata) {
				var functext = vardata.valueFunction;
				// now sanitise any other variables that may occur in this function:
				functext = varNameSubstitutions(functext,facilitymetadata);
				functext = ensureFunctionHeader(functext);
				// add sannitizes function to the evaluation context
				ctx.set(variablename, functext);
			}
			if("validFunction" in vardata) {
				var functext = vardata.validFunction;
				// console.log("raw "+variablename+" validFunction:"+functext);
				// sanitise:
				functext = varNameSubstitutions(functext,facilitymetadata);
				functext = ensureFunctionHeader(functext);
				// add to scope under another name:
				// console.log(facilitymetadata);
				ctx.set(variablename+"_valid", functext);
			}
		});
	};

	ctx.updateEvalContext = function(facilitydata) {
		$.each(facilitydata, function(variablename, vardata) {
			// if(vardata.data) {
			if(typeof(vardata.value)=="string") {
				ctx.set(variablename,"\""+vardata.value+"\"");
			} else if(typeof(vardata.value)=="number" || typeof(vardata.value)=="boolean") {
				ctx.set(variablename,vardata.value);
			} else if(typeof(vardata.value)=="undefined") {
				// in case previously submitted data goes missing we write the new undefined value to the context:
				ctx.set(variablename,vardata.value);
			} else {
				console.warn("skipping update of "+variablename+", because it has an unusual datatype: "+typeof(vardata.value));
			}
		});
	};

	// console.log(ctx);
	return ctx;
};


/* helper that replaces all known variables by a jsFriendlyVariablename() and adds () if they are themselves derived */
function varNameSubstitutions(functext, facilitymetadata) {
	// var oldtxt = functext;
	// console.log("transcribing: "+functext);
	// very basic js tokenizer:
	var tokens = functext.split(/([^a-zA-Z0-9:_$])/);
	// in order to not make silly substitutions we only replace variables if they occur as a whole token (inculding : because it is allowed in parameter names)
	// next translate each token (as a whole or not at all)
	tokens = $.map(tokens, function(token) {
		// extract variablename
		var varname = token.endsWith("_valid")?token.slice(0,-6):token;
		// if token not a known variablename, then keep token "as is"
		if(!(varname in facilitymetadata)) {
			return token;
		}

		if(token.endsWith("_valid") || 'valueFunction' in facilitymetadata[varname]) {
			// return jsFriendlyVariablename(token)+"()";
            return "get(\'"+token+"\')";
		} else {
			return token;
		}
	});
	// console.log("end result: "+tokens.join(""));
	return tokens.join("");
}


/* simple helper to decide whether or not something includes the function(){ header and add it if it isn't there */
function ensureFunctionHeader(functext) {
	try {
		var f;
		eval("f="+functext);
		if(typeof(f)!="function") {
			throw "it evals correctly but it isn't a function";
		}
	} catch(e) {
		// console.log("adding function() header to \""+functext+"\"");
		// when evaluation failed or did not return a function, then wrap it in a function
		functext = "function(){return "+functext+"}";
	}
	return functext;
}




