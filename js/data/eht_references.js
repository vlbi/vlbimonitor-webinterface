
{"type": "FeatureCollection",
 "features": [
 	{ "type": "Feature", "properties":{"name":"0006-063"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [   1.55788666666666666665 ,   -5.60685138888888888890 ]}, "id": "0006-063"  },
 	{ "type": "Feature", "properties":{"name":"0102+584"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  15.69067624999999999995 ,   58.40309333333333333333 ]}, "id": "0102+584"  },
 	{ "type": "Feature", "properties":{"name":"W3(OH)"    , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  36.76596249999999999990 ,   61.87354722222222222221 ]}, "id": "W3(OH)"    },
 	{ "type": "Feature", "properties":{"name":"3C84"      , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  49.95066708333333333315 ,   41.51169527777777777777 ]}, "id": "3C84"      },
 	{ "type": "Feature", "properties":{"name":"0359+509"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  59.87394666666666666660 ,   50.96393361111111111111 ]}, "id": "0359+509"  },
 	{ "type": "Feature", "properties":{"name":"L1551-IR"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  67.89224999999999999985 ,   18.13475833333333333333 ]}, "id": "L1551-IR"  },
 	{ "type": "Feature", "properties":{"name":"3C120"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  68.29623124999999999995 ,    5.35433861111111111111 ]}, "id": "3C120"     },
 	{ "type": "Feature", "properties":{"name":"0510+180"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  77.50987124999999999985 ,   18.01155027777777777777 ]}, "id": "0510+180"  },
 	{ "type": "Feature", "properties":{"name":"0522-364"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  80.74160249999999999985 ,  -35.54143055555555555556 ]}, "id": "0522-364"  },
 	{ "type": "Feature", "properties":{"name":"OMC1"      , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  83.80988749999999999995 ,   -4.62434722222222222223 ]}, "id": "OMC1"      },
 	{ "type": "Feature", "properties":{"name":"N2071IR"   , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [  86.77021249999999999995 ,    0.36308333333333333333 ]}, "id": "N2071IR"   },
 	{ "type": "Feature", "properties":{"name":"OJ287"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 133.70364541666666666665 ,   20.10851138888888888888 ]}, "id": "OJ287"     },
 	{ "type": "Feature", "properties":{"name":"IRC+1021"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 146.98909166666666666655 ,   13.27879444444444444443 ]}, "id": "IRC+1021"  },
 	{ "type": "Feature", "properties":{"name":"1058+015"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 164.62335499999999999980 ,    1.56633972222222222222 ]}, "id": "1058+015"  },
 	{ "type": "Feature", "properties":{"name":"3C273"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 187.27791541666666666650 ,    2.05238833333333333333 ]}, "id": "3C273"     },
 	{ "type": "Feature", "properties":{"name":"M87"       , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 187.70593083333333333330 ,   12.39112333333333333333 ]}, "id": "M87"       },
 	{ "type": "Feature", "properties":{"name":"3C279"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 194.04652749999999999990 ,   -4.21068750000000000001 ]}, "id": "3C279"     },
 	{ "type": "Feature", "properties":{"name":"CENA"      , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 201.36506458333333333320 ,  -42.98088777777777777779 ]}, "id": "CENA"      },
 	{ "type": "Feature", "properties":{"name":"1512-090"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 228.21055374999999999990 ,   -8.90004750000000000001 ]}, "id": "1512-090"  },
 	{ "type": "Feature", "properties":{"name":"16293-24"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 248.09545416666666666660 ,  -23.52344444444444444446 ]}, "id": "16293-24"  },
 	{ "type": "Feature", "properties":{"name":"G343.0"    , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 254.57139999999999999990 ,  -41.13149722222222222223 ]}, "id": "G343.0"    },
 	{ "type": "Feature", "properties":{"name":"NGC6334I"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 260.22268749999999999990 ,  -34.21620277777777777779 ]}, "id": "NGC6334I"  },
 	{ "type": "Feature", "properties":{"name":"SGRA"      , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 266.41681708333333333320 ,  -28.99217583333333333334 ]}, "id": "SGRA"      },
 	{ "type": "Feature", "properties":{"name":"1749+096"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 267.88674416666666666655 ,    9.65020222222222222222 ]}, "id": "1749+096"  },
 	{ "type": "Feature", "properties":{"name":"G5.89"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 270.12656666666666666655 ,  -23.93320000000000000001 ]}, "id": "G5.89"     },
 	{ "type": "Feature", "properties":{"name":"G10.62"    , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 272.61942083333333333310 ,  -18.06951111111111111112 ]}, "id": "G10.62"    },
 	{ "type": "Feature", "properties":{"name":"G34.3"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 283.32737083333333333320 ,    1.24951666666666666666 ]}, "id": "G34.3"     },
 	{ "type": "Feature", "properties":{"name":"G45.1"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 288.34199583333333333315 ,   10.84817222222222222221 ]}, "id": "G45.1"     },
 	{ "type": "Feature", "properties":{"name":"1924-292"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 291.21273291666666666660 ,  -28.75829972222222222223 ]}, "id": "1924-292"  },
 	{ "type": "Feature", "properties":{"name":"CYG-A"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 299.86820333333333333325 ,   40.73390194444444444444 ]}, "id": "CYG-A"     },
 	{ "type": "Feature", "properties":{"name":"K3-50"     , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 300.44037083333333333310 ,   33.54542222222222222221 ]}, "id": "K3-50"     },
 	{ "type": "Feature", "properties":{"name":"2015+371"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 303.86970708333333333330 ,   37.18319833333333333332 ]}, "id": "2015+371"  },
 	{ "type": "Feature", "properties":{"name":"W75N"      , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 309.65180416666666666650 ,   42.62624722222222222221 ]}, "id": "W75N"      },
 	{ "type": "Feature", "properties":{"name":"CRL2688"   , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 315.57835416666666666660 ,   36.69380555555555555555 ]}, "id": "CRL2688"   },
 	{ "type": "Feature", "properties":{"name":"2232+117"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 338.15170374999999999990 ,   11.73080666666666666666 ]}, "id": "2232+117"  },
 	{ "type": "Feature", "properties":{"name":"3C454.3"   , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 343.49061624999999999990 ,   16.14821111111111111110 ]}, "id": "3C454.3"   },
 	{ "type": "Feature", "properties":{"name":"N7538IRS"  , "type":"pos", "mag": 0, "dim": "60"}, "geometry":{ "type":"Point", "coordinates": [ 348.43894166666666666655 ,   61.45554444444444444444 ]}, "id": "N7538IRS"  }
]}

