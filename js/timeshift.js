

var date_delta = 0;

bkp_date = Date;
bkp_date_proto = Date.prototype;
Date = function()
{
	switch (arguments.length)
	{
		// normally we'd use Function.prototype.apply for this but that does not work
		// on constructor functions called via the "new" keyword. There exists a work-around
		// (see here: https://nullprogram.com/blog/2013/03/24/) but that does not work
		// for built-in types including Date
		case 1: return new bkp_date(arguments[0]); break;
		case 2: return new bkp_date(arguments[0], arguments[1]); break;
		case 3: return new bkp_date(arguments[0], arguments[1], arguments[2]); break;
		case 4: return new bkp_date(arguments[0], arguments[1], arguments[2], arguments[3]); break;
		case 5: return new bkp_date(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]); break;
		case 6: return new bkp_date(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]); break;
		case 7: return new bkp_date(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]); break;
		case 8: return new bkp_date(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7]); break;
		// if no arguments, then current time is requested and we substitute a delta:
		case 0: return new bkp_date(new bkp_date().getTime()+date_delta); break;
	}
}
Date.prototype = bkp_date.prototype;
// Date has 2 alternative constructors
Date.parse = bkp_date.parse;
Date.UTC   = bkp_date.UTC;
//*/

var streamtimeSkewTimer;
function setStreamtime(unixtimestamp_ms)
{
	clearTimeout(streamtimeSkewTimer);
	var newdelta = new bkp_date(unixtimestamp_ms)- new bkp_date();
	// always allow stepping forward
	if (newdelta > date_delta || newdelta < date_delta - 10000) {
		console.log("warping time offset "+newdelta/1000 + "s; was " + date_delta/1000 + "s; delta=" + (newdelta - date_delta)/1000 + "s");
		date_delta = newdelta;
	} else {
		console.log("morphing time offset "+newdelta/1000 + "s; was " + date_delta/1000 + "s; delta=" + (newdelta - date_delta)/1000 + "s");
		// slowly morph:
		var morph = function () {
			if (newdelta > date_delta)
			{
				date_delta += 10;
			}
			else
			{
				date_delta -= 10;
			}
			// if there is still skew the  schedule another update
			if (Math.abs(date_delta - newdelta) >= 10)
				streamtimeSkewTimer = setTimeout(morph, 100);
		};
		morph();
	}
}
