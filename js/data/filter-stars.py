#!/usr/bin/env python3

import json
import sys

olddata = json.load(sys.stdin)

newdata = {
	"type": "FeatureCollection",
	"features": [
		x for x in olddata["features"] if x["properties"]["mag"] <= 3.5
	]
}

print(json.dumps(newdata))
